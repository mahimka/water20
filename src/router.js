import createRouter from 'router5'

import browserPlugin from 'router5-plugin-browser'


const routes = [
    { name: 'main', path: '/main'},
    { name: 'person', path: '/person/:person'},
];

const router = createRouter(routes, {
    defaultRoute:'main'
})

// doesnt really work in the ria endless
router.usePlugin(
    browserPlugin({
        useHash: true
    })
)

router.start()

export default router