import React, { Component, Fragment,  useRef, useState, useEffect, useContext } from 'react'
import { useCallback, useMemo } from 'use-memo-one'
import { useRouter, useRoute } from 'react-router5'

import ScreenHeight from '@ips/react/components/screen-height'
import Pic from '@ips/react/components/pic'
import Overlay from '@ips/react/components/overlay'
import Vidos from '@ips/react/components/vidos'

import Text from './text'
import { Slice, SliceBox, SliceCon, Column, Row, useGridStyle } from '@ips/react/components/layout'

import { useActivation } from '@ips/react/components/activation'
import { useSizerSize } from '@ips/react/components/utils/use-sizer'
import cx from '@ips/app/classnamex'
import * as __ from '@ips/app/hidash'


import './popup.styl'

// TODO: consider moving Text's content to the children
export const Cover = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    // if(mobile)
    //     return (<Slice className={cx("popup-cover", mobile&&"mobbd")}>
    //                 <Pic cover mode="img" src={p.coverm||p.cover}/>
    //                 <Text textStyle="popup-cover-title" text={p.name}/>
    //             </Slice>)

    return (<SliceBox className="popup-cover">
                {/*<Overlay cover mode="background" src={p.cover+'.jpg'}/>*/}
                <Overlay cover>
                    <Vidos mode="background" srcId={p.cover} autoPlay loop muted/>
                </Overlay>
                <SliceCon minHeight={mobile?'50vh':"screen"} column valign="center">
                        <Pic src={p.coverLogo}/>
                </SliceCon>  
            </SliceBox>)
}

export const DesktopOnly = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0
    return <Fragment>{(!mobile) ? p.children : null}</Fragment>
}

export const MobileOnly = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0
    return <Fragment>{(mobile) ? p.children : null}</Fragment>
}

export const BodyBlock = p=>{
    return (<SliceBox className={`body-block ${p.reversed? 'reversed':''}`}>
                <SliceCon>
                    {p.children}
                </SliceCon>
            </SliceBox>)
}

export const LustraBlock = p=>{

    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    const w = +p.width||7
    const l = __.ud(p.left)?3:+p.left

    return (<Slice padding={mobile?"36px 0px":"96px 0px"} column={mobile} reverse={mobile}>
                {!mobile? <Column width={w+2} left={l} right={12-w-l-2}   height="2px">
                    <Overlay cover backgroundColor="#8C7F50"/>
                </Column> : null }
                <Column width={l+2} valign="space-between">
                    <Column width={mobile?"50%":"2"} left={mobile?'50%':l} padding="14px 0px">
                        <Text textStyle="lustra" text={p.text}/>
                    </Column>
                    {p.children}
                </Column>
                <Column width={w}>
                    <Pic src={p.src} res2x noGutter/>
                </Column>
                {p.after}
            </Slice>)
}

export const Lead = p=>{

    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    return (<Column width={p.width||6} left={__.ud(p.left)?"2":p.left} padding={ mobile? "48px 0px 36px 0px": "98px 0px 36px 0px" }>
                <Text textStyle="lead" text={p.text}/>
            </Column>)
}

export const Quote = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    return (<Slice className="quote" column  padding={mobile?"36px 0px 48px 0px":"64px 0px 96px 0px"}>
                <Column left="2" width="8">
                    <Pic src={p.imgTop} res2x/>
                </Column>
                <Column left="2" width="8" padding="24px 0px 36px 0px">
                    <Text textStyle="quote" text={p.text}/>
                </Column>
                <Column left="3" width="6" padding="0 0px 36px 0px">
                    <Text textStyle="quote-desc" text={p.desc}/>
                </Column>
                <Column left="2" width="8">
                    <Pic src={p.imgBottom} res2x/>
                </Column>
            </Slice>)
}

export const Factoid = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    return mobile?null:(<Column left={p.left} width="3" valign={p.valign} className="facto">
                <Pic src={p.img} noGutter res2x/>
            </Column>)
}

export const After75 = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    return (<Slice padding="96px 0px 128px 0px">
                <Pic noGutter src={mobile?"75ylater-m.svg":"75ylater.png"} padding="0px 0px 80px 0px "/>
                <Column width="6">
                    <Pic res2x noGutter src={p.img}/>
                </Column>
                <Column width="5" padding={mobile?"30px 0 0 0":null}>
                    <Text textStyle="bodysh" text={p.text}/>
                </Column>
            </Slice>)
}

export const Footer = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0
    // /className={cx("footer", mobile&&"mobbd")}
    return (<SliceBox >
                {/*<Overlay cover backgroundColor="#F7F2EC"/>*/}
                <SliceCon padding="48px 0px 64px 0px">
                    { p.children }
                    <Column left="3" width="6">
                        <Text textStyle="footer-legal" mod="footer-legal" text="<p>Проект подготовлен при участии<br>Департамента инвестиционной и промышленной политики города Москвы</p><p>МИА «Россия сегодня», 2020 год</p>"/>
                    </Column>                        
                </SliceCon>
            </SliceBox>)
}

export const TextButton = p=>{
    const gridStyle = useGridStyle()
    const gutterClass = gridStyle.gutterClass()

    return <div className={cx("button", p.mod||'', gutterClass)} onClick={p.onClick}><div className="buttoncon"><Text noGutter textStyle={"button-"+(p.mod||'')} text={p.text}/></div></div>
}

export const Card = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0
    const active = useActivation()

    const content = useMemo(()=>(
        <Column padding="20px" width={mobile?"auto":"378px"}>
                {/*<Pic className="thumb" noGutter res2x src={p.thumb||p.cthumb}/>*/}
                <div>
                    <Vidos srcId={p.covert?p.covert:(p.cover+'t')} autoPlay={active} loop muted/>
                    { p.covertl ? <Fragment>
                                        <Overlay cover backgroundColor="rgba(0,0,0,0.5)"/>
                                        <Overlay cover mode="img" src={p.covertl}/>
                                </Fragment>
                        : null }
                </div>
                <Row valign="center" padding="12px 0px">
                    <Pic noGutter src={p.icon} maxWidth="7%"/>
                    <Text textStyle="pop-arms" text={p.arms} padding="0px 0px 0px 8px"/>
                </Row>
                <Text textStyle="pop-name" text={p.name} padding="0px"/>
                <Text textStyle="pop-location" text={p.location} padding="15px 0px 20px 0px"/>
                <Text textStyle="pop-desc" text={p.desc} padding="0px" padding="0px 0px 20px 0px"/>
            </Column>
        ),[active])

    return p.pop?(
        <Overlay 
            onClick={p.onClick}
            className={cx("map-kk", 'hidey', active&&'active')} 
            backgroundColor="white" 
            style={{
                boxShadow: '0px 14px 34px rgba(0, 0, 0, 0.25)',
                left: `${p.tx*100}%`,
                top: `${p.ty*100}%`,
                transform: `translate(-${p.tx*100}%, -${p.ty*100}%)`,
            }}
            maxWidth="50%">
            {content}
        </Overlay>):(
        <Column
            onClick={p.onClick}
            className={cx("map-kk", active&&'active')} 
            minHeight="90vh"
            >
            <Overlay
            cover 
            backgroundColor="white" 
            style={{
                boxShadow: '0px 14px 34px rgba(0, 0, 0, 0.25)',
                left: `${p.tx*100}%`,
                top: `${p.ty*100}%`,
                transform: `translate(-${p.tx*100}%, -${p.ty*100}%)`,
            }}/>
            {content}
        </Column>)

}

export const MobileCard = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0
    const active = useActivation()

    const content = useMemo(()=>(
        <Column padding="20px 0px 30px 0px" valign="space-between" minHeight="100%">
            <div>
                <Text textStyle="pop-name" text={p.name} padding="0px 15px" style={{minHeight:"4em"}}/>
                <Text textStyle="pop-location" text={p.location} padding="8px 15px 20px 15px"/>
                {/*<Pic className="thumb" noGutter res2x src={p.thumb||p.cthumb}/>*/}
                <div>
                    <Vidos srcId={p.covert?p.covert:(p.cover+'t')} autoPlay={active} loop muted/>
                    { p.covertl ? <Fragment>
                                        <Overlay cover backgroundColor="rgba(0,0,0,0.5)"/>
                                        <Overlay cover mode="img" src={p.covertl}/>
                                </Fragment>
                        : null }
                </div>
                <Text textStyle="pop-desc" text={p.desc} padding="0px" padding="15px 15px 20px 15px"/>
            </div>
            <Row left="10%" width="80%" valign="center">
                <TextButton mod="forward" text="СМОТРЕТЬ"/>
            </Row>
        </Column>
        ),[active])

    return (
        <Column
            onClick={p.onClick}
            className={cx("map-kk", active&&'active')} 
            minHeight="90vh"
            height="90vh"
            >
            <Overlay
            cover 
            backgroundColor="white" 
            style={{
                boxShadow: '0px 14px 34px rgba(0, 0, 0, 0.25)',
                left: `${p.tx*100}%`,
                top: `${p.ty*100}%`,
                transform: `translate(-${p.tx*100}%, -${p.ty*100}%)`,
            }}/>
            {content}
        </Column>)

}

export const OthersSlide = p=>{
    trace('cthumb', p.cthumb)
    return useMemo(()=>(<Column className="others-slide" onClick={p.onClick}>
                <div className="kortina">
                    <Pic mode="background" res2x src={p.cthumb||p.thumb} aspect="0.65333333"/>
                </div>
                <div className="infos">
                    <Text textStyle="others-text" text={p.name} padding="10px 0 0 0"/>
                    {/*<Text mod="odesc" text={p.desc}/>*/}
                </div>
            </Column>),[p.cthumb||p.thumb, p.name, p.onClick])
}
