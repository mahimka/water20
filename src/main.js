import React, { Fragment, useRef,  useState, useEffect } from 'react'
import { useMemo, useCallback } from 'use-memo-one'

import viewportUnitsBuggyfill from 'viewport-units-buggyfill'
viewportUnitsBuggyfill.init();

import './main.styl'
import * as __ from '@ips/app/hidash'
import cx from '@ips/app/classnamex'


import Water from './water'

import * as Metrika from '@ips/app/metrika'
import * as GTM from '@ips/app/google-tag-manager'

function anaEvent(event, params){
    GTM.event(event, params)
    Metrika.event(event, params)
}

const WrMain = p=>(<Water />)

export default WrMain