import React, { Suspense, lazy, Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'


import Pic from '@ips/react/components/pic'
import Overlay from '@ips/react/components/overlay'
import Sticky from '@ips/react/components/sticky'
import { useSizerSize } from '@ips/react/components/utils/use-sizer'
import { useSizerResize } from '@ips/react/components/utils/use-resize'
import { Slice, SliceBox, SliceCon, Column, Row } from '@ips/react/components/layout'
import TextStyle from '@ips/react/components/text-style'
import { ListActivator } from '@ips/react/components/list-activator'
import { useActivation } from '@ips/react/components/activation'
import createEventTargetHook from '@ips/react/components/utils/create-event-target-hook'
import Text from './text'
import cx from '@ips/app/classnamex'


import './water.styl'
import DATA from 'data/data.json';
import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import {EasePack, ScrollToPlugin} from 'gsap/all'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Lottie from 'react-lottie';

if (typeof window !== "undefined") {
  gsap.registerPlugin(ScrollToPlugin); 
}

// import P1 from 'parts/part1'

// const P1 = lazy(() => import(`parts/part1`));
// const P2 = lazy(() => import(`parts/part2`));
// const P3 = lazy(() => import(`parts/part3`));
// const P4 = lazy(() => import(`parts/part4`));
// const P5 = lazy(() => import(`parts/part5`));

const Pp = lazy(() => import(`parts/parts`));

const Bg = p=>{

    return (
        <div className={`wtr-bg`}>
            <Pic className={`wtr-bg-png`} noGutter src="bg.png" maxWidth="100%"/>
  

            {
                (!p.mobile) ? 
                    <Suspense fallback={``}>
                        
                        <Pp part={p.part} spart={p.spart} autoplay={false} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded} />
                        
                    </Suspense>
                    :
                    <div className={`wtr-parts`} />
            }
        </div>
    )
}

const Elements = p=>{
    let element = <div/>

    if(p.data.type == "h1"){
        element = <p className={`wtr-h1`} dangerouslySetInnerHTML={{__html: p.data.value}} />
    }

    if(p.data.type == "h0"){
        element = <p className={`wtr-h0`} dangerouslySetInnerHTML={{__html: p.data.value}} />
    }

    if(p.data.type == "percent1"){
        element = <div className={`wtr-percent`} dangerouslySetInnerHTML={{__html: `<svg width="325" height="10" viewBox="0 0 325 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M64.0592 4.67533C64.9481 3.84416 65.0469 1.21212 64.9852 -1.35557e-09L5 -5.48746e-06C2.23857 -5.74001e-06 -4.67672e-08 2.23857 -1.04457e-07 5C-1.62148e-07 7.76142 2.23857 10 5 10L63.1795 10C63.1023 8.57143 63.1702 5.5065 64.0592 4.67533Z" fill="#9EE4F6"/>
        <path d="M320 0.5C322.485 0.5 324.5 2.51472 324.5 5.00001C324.5 7.4853 322.485 9.50003 320 9.50003L5 9.5C2.51471 9.5 0.5 7.48528 0.5 4.99999C0.5 2.51469 2.51474 0.499971 5 0.499971L320 0.5Z" stroke="#364659"/>
        </svg>`}} />
    }

    if(p.data.type == "percent2"){
        element = <div className={`wtr-percent`} dangerouslySetInnerHTML={{__html: `<svg width="325" height="10" viewBox="0 0 325 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M128.118 4.67533C129.896 3.84416 130.094 1.21212 129.97 -2.71113e-09L5 -1.14322e-05C2.23858 -1.16848e-05 -4.67672e-08 2.23857 -1.04457e-07 4.99999C-1.62148e-07 7.76142 2.23858 9.99999 5.00001 9.99999L126.359 10C126.205 8.57143 126.34 5.5065 128.118 4.67533Z" fill="#9EE4F6"/>
        <path d="M320 0.5C322.485 0.5 324.5 2.51472 324.5 5.00001C324.5 7.4853 322.485 9.50003 320 9.50003L5 9.5C2.51471 9.5 0.5 7.48528 0.5 4.99999C0.5 2.5147 2.51474 0.499971 5 0.499971L320 0.5Z" stroke="#364659"/>
        </svg>`}} />
    }

    if(p.data.type == "percent3"){
        element = <div className={`wtr-percent`} dangerouslySetInnerHTML={{__html: `<svg width="325" height="10" viewBox="0 0 325 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M192.178 4.67533C194.844 3.84416 195.141 1.21212 194.956 -4.0667e-09L5 -1.7377e-05C2.23857 -1.76295e-05 -4.67672e-08 2.23856 -1.04457e-07 4.99999C-1.62148e-07 7.76141 2.23857 9.99999 5 9.99999L189.538 10C189.307 8.57143 189.511 5.5065 192.178 4.67533Z" fill="#9EE4F6"/>
        <path d="M320 0.5C322.485 0.5 324.5 2.51472 324.5 5.00001C324.5 7.4853 322.485 9.50003 320 9.50003L5 9.5C2.51471 9.5 0.5 7.48528 0.5 4.99999C0.5 2.51469 2.51474 0.499971 5 0.499971L320 0.5Z" stroke="#364659"/>
        </svg>`}} />
    }

    if(p.data.type == "percent4"){
        element = <div className={`wtr-percent`} dangerouslySetInnerHTML={{__html: `<svg width="325" height="10" viewBox="0 0 325 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M256.237 4.67533C259.793 3.84416 260.188 1.21212 259.941 -5.42227e-09L5 -2.33217e-05C2.23859 -2.35743e-05 -4.67672e-08 2.23855 -1.04457e-07 4.99998C-1.62148e-07 7.7614 2.23859 9.99998 5 9.99998L252.718 10C252.409 8.57143 252.681 5.5065 256.237 4.67533Z" fill="#9EE4F6"/>
        <path d="M320 0.5C322.485 0.5 324.5 2.51472 324.5 5.00001C324.5 7.4853 322.485 9.50003 320 9.50003L5 9.5C2.51471 9.5 0.5 7.48528 0.5 4.99999C0.5 2.51469 2.51474 0.499971 5 0.499971L320 0.5Z" stroke="#364659"/>
        </svg>`}} />
    }

    if(p.data.type == "percent5"){
        element = <div className={`wtr-percent`} dangerouslySetInnerHTML={{__html: `<svg width="325" height="10" viewBox="0 0 325 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M320 0.5C322.485 0.5 324.5 2.51472 324.5 5.00001C324.5 7.4853 322.485 9.50003 320 9.50003L5 9.5C2.51471 9.5 0.5 7.48528 0.5 4.99999C0.5 2.51469 2.51474 0.499971 5 0.499971L320 0.5Z" fill="#9EE4F6" stroke="#364659"/>
        </svg>`}} />
    }


    if(p.data.type == "p"){
        element = <p className={`wtr-p`} dangerouslySetInnerHTML={{__html: p.data.value}} />
    }

    if(p.data.type == "p2"){
        element = <p className={`wtr-p2`} dangerouslySetInnerHTML={{__html: p.data.value}} />
    }    

    if(p.data.type == "pv"){
        element = <p className={`wtr-pv`} dangerouslySetInnerHTML={{__html: p.data.value}} />
    }    

    if(p.data.type == "pm"){
        element = <p className={`wtr-pm`} dangerouslySetInnerHTML={{__html: p.data.value}} />
    }    

    if(p.data.type == "val"){
        element = <p className={`wtr-val`} data-id={p.data.id} dangerouslySetInnerHTML={{__html: p.data.value}} />
        {/*
                    <Waypoint 
                    scrollableAncestor={window} 
                    bottomOffset={"25%"} 
                    fireOnRapidScroll={false}
                    debug={false}
                    onEnter={(e) => {
                        gsap.killTweensOf([`.wtr-val[data-id="${p.data.id}"] > span`]);
                        gsap.set(`.wtr-val[data-id="${p.data.id}"] > span`, {opacity:0, scale: 0.8});
                        gsap.to(`.wtr-val[data-id="${p.data.id}"] > span`, {opacity:1, scale: 1, duration:0.5, ease: "power2.out"});
                    }}
                    onLeave={(e) => {
                        gsap.killTweensOf([`.wtr-val[data-id="${p.data.id}"] > span`]);
                        gsap.to(`.wtr-val[data-id="${p.data.id}"] > span`, {opacity:0, scale: 0.8, duration:0.2, ease: "power2.out"});
                    }}
                  >
                  <p className={`wtr-val`} data-id={p.data.id} dangerouslySetInnerHTML={{__html: p.data.value}} />
                  </Waypoint>
        */}
    }

    if(p.data.type == "img"){
        element = <Fragment>
        <Pic src={`img` + p.data.id + `.jpg`} maxWidth="100%" res2x/>
        <p className={`wtr-alt-p`} data-id={p.data.id}>{p.data.alt}</p>
        </Fragment>
    }

    if(p.data.type == "imgsvg"){
        element = <div className="wtr-imgw">
        <Pic src={`img` + p.data.id + `.svg`} maxWidth="100%"/>
        </div>
    }    

    if(p.data.type == "imgw"){
        element = <div className="wtr-imgw">
        <Pic src={`img` + p.data.id + `.jpg`} maxWidth="100%" res2x/>
        </div>
    }

    if(p.data.type == "imgm"){
        element = <div className="wtr-imgm">
        <Pic src={`img` + p.data.id + `.png`} maxWidth="100%" res2x/>
        </div>
    }    

    if(p.data.type == "imgp"){
        element = <Fragment>
        <Pic src={`img` + p.data.id + `.png`} maxWidth="100%" res2x/>
        </Fragment>
    }    

    return (
        element
    )
}

const Navigator = p=>{
    return (
        <div className={`wtr-navigator`}>
            <div className={`wtr-navigator-waves`}>
                <div className={`wtr-navigator-wave`} data-id={"0"}>
                    <svg className={`wtr-navigator-wave-svg`} data-id={`0`} x="0px" y="0px" viewBox="0 0 100 19" width="100" height="19">
                        <path fill="#6ABFE5" d="M0.25,18.3C16.87,18.3,16.87,1,33.5,1c16.63,0,16.63,17.3,33.25,17.3S83.37,1,100,1v18.2L0.5,19.5L0.25,18.3z"/>
                    </svg>
                    <div className={`wtr-navigator-wave-div`} data-id={`0`}/>
                </div>
                <div className={`wtr-navigator-wave`} data-id={"1"}>
                    <svg className={`wtr-navigator-wave-svg`} data-id={`1`} x="0px" y="0px" viewBox="0 0 100 19" width="100" height="19">
                        <path fill="#A0D9F7" d="M0.25,18.3C16.87,18.3,16.87,1,33.5,1c16.63,0,16.63,17.3,33.25,17.3S83.37,1,100,1v18.2L0.5,19.5L0.25,18.3z"/>
                    </svg>
                    <div className={`wtr-navigator-wave-div`} data-id={`1`}/>
                </div>
            </div>
            <div className={`wtr-nav-buttons`}>
            {
                DATA.sections.map((d,i)=>(
                    <div className={`wtr-nav-btn`} key={i} data-id={i} 
                        onClick={(props) => {
                            gsap.to(window, {duration: 1, scrollTo: {y:`.wtr-windows[data-id="${4-i}"]`, offsetY: 0}, ease: "power2.inOut"});
                        }}
                    />
                ))
            }
            </div>
        </div>
    )
}

const Water = p=>{

    return (<Column>
                <SliceBox className="wtr20">
                    
                    <Overlay cover>
                        <Sticky>
                            <Column width="100%" minHeight="100vh" minHeight="screen" align="center" className={`wtr-move`}>
                                <Bg part={p.part} spart={p.spart} mobile={p.mobile} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded}/>
                                
                                <div className={`wtr-headers`}>
                                {
                                    DATA.sections.map((d,i)=>(
                                        <p className={`wtr-header` + (( i == p.part)?` wtr-header-current`:``)} key={i} data-id={i} >
                                            {d.name}
                                        </p>
                                    ))
                                }
                                </div>

                                <Navigator />
                            </Column>
                        </Sticky>
                    </Overlay>



                    <SliceCon className={`wtr-folder`} align-self="center" valign="space-between" column width="12" minHeight="screen" padding={"32px 0px 32px 0px"}>
                        <Overlay cover className="wtr-folder-bg" mode="background" backgroundSize="cover" backgroundRepeat="no-repeat" src="obl.jpg" />
                        <div className={`wtr-folder-text`} data-id="0">{`Московская вода:`}</div>
                        <div className={`wtr-folder-text`} data-id="1">{`что мы пьем и сливаем в реки`}</div>
                        <div className={`wtr-folder-text-m`}>
                            <div className={`wtr-folder-span`} data-id="0">{`Московская`}</div>
                            <div className={`wtr-folder-span`} data-id="1">{`вода: что`}</div>
                            <div className={`wtr-folder-span`} data-id="2">{`мы пьем`}</div>
                            <div className={`wtr-folder-span`} data-id="3">{`и сливаем`}</div>
                            <div className={`wtr-folder-span`} data-id="4">{`в реки`}</div>
                        </div>
                    </SliceCon>



                    <div className={`wtr-start`}>
                        <span>
                            {`Многие блага цивилизации стали для нас привычными. Среди них — вода, которая потечет из крана, стоит его открыть. Но мало кто задумывается, какой сложный путь она проходит, прежде чем попасть в наши квартиры, и что ее ждет после выхода из домов`}
                        </span>
                    </div>



                    {
                        DATA.sections.map((d,i)=>(
                           
                            <Waypoint 
                                scrollableAncestor={window} 
                                bottomOffset={"50%"} 
                                fireOnRapidScroll={false}
                                debug={false}
                                key={i}
                                onPositionChange={(e) => {}}
                                onLeave={(props) => {}}
                                onEnter={(e) => {
                                    if(p.loaded){
                                        gsap.killTweensOf([`.wtr-navigator-waves`]);
                                        gsap.to(`.wtr-navigator-waves`, {y:`${81 - i*20}%`, duration:1});
                                        

                                        if(p.part != i){
                                            if(p.part < i){
                                                p.setsPart(0)
                                            }else{
                                                p.setsPart(20)
                                            }
                                            p.setPart(i)


                                        }
                                        

                                    }
                                }}
                              >

                                <div 
                                  className={`wtr-windows`}
                                  data-id={i}
                                >
                            
                                {
                                    d.sections.map((q,j)=>(
                                        <Column
                                          className={`wtr-win-column wtr-win-column${i}`}
                                          left={p.mobile?0:0}
                                          right={p.mobile?0:0}
                                          width={p.mobile?12:12}
                                          key={i+`-`+j}
                                        >
                                            <Waypoint 
                                                scrollableAncestor={window} 
                                                bottomOffset={"50%"} 
                                                fireOnRapidScroll={false}
                                                debug={false}
                                                onEnter={(e) => {
                                                    if(p.loaded){
                                                        p.setsPart(j)
                                                        if(i!=p.part){
                                                            gsap.killTweensOf([`.wtr-navigator-waves`]);
                                                            gsap.to(`.wtr-navigator-waves`, {y:`${81 - i*20}%`, duration:1});
                                                            p.setPart(i)
                                                        }
                                                    }
                                                }}
                                              >
                                                <div data-id={q.type} className={`wtr-window`}>
                                                {
                                                    q.content.map((w,k)=>(
                                                        <Elements key={i+`-`+j+`-`+k} data={w} did={i+"-"+j}/>
                                                    ))
                                                }
                                                </div>
                                            </Waypoint>

                                        </Column>
                                    ))
                                }

                                </div>
                            </Waypoint>
                           
                          ))
                    }

                    <SliceCon className={`wtr-footer`} align-self="center" column width="12" minHeight="screen" valign="space-between">
                        <Column className={`wtr-footer-top`} left={p.mobile?0:3} width={p.mobile?12:6} valign="center" align="center">
                            <p>{`Технологии очистки питьевой воды и стоков постоянно совершенствуются. В столице отказались от использования жидкого хлора, вызывавшего множество нареканий, применяют озоносорбцию, мембранную ультрафильтрацию и другие передовые методы.`}</p>
                            <p>{`В итоге вода из-под крана соответствует не только российским, но и мировым стандартам, а городские стоки не загрязняют окружающую среду`}</p>

                            <div className={`wtr-footer-pic`}>
                                <Lottie 
                                    options={{
                                        loop: true,
                                        autoplay: true, 
                                        animationData: DATA.animations[0],
                                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                                    }}
                                    height={281}
                                    width={350}
                                    isStopped={false}
                                    isPaused={false}
                                />
                            </div>

                        </Column>
                        <Column width="12" className={`wtr-footer-bottom`}>
                            <Column width={p.mobile?12:6} left={p.mobile?0:3}>
                                <SliceCon>
                                    <Column width={p.mobile?12:3}>
                                        <p className={`wtr-footer-p`} data-id={"0"} dangerouslySetInnerHTML={{__html: `Спецпроект подготовлен при&nbsp;участии компании «Мосводоканал»`}} />
                                    </Column>
                                    <Column width={p.mobile?12:3}>
                                        <p className={`wtr-footer-p`} data-id={"1"} dangerouslySetInnerHTML={{__html: `МИА «Россия сегодня»<br>2020&nbsp;год`}} />
                                        <p className={`wtr-footer-p`} data-id={"2"} dangerouslySetInnerHTML={{__html: `МИА «Россия сегодня», 2020 год`}} />
                                    </Column>
                                </SliceCon>
                            </Column>
                        </Column>
                    </SliceCon>

                </SliceBox>
            </Column>
        )
}

export default p=>{

    const sedda = ()=>{
        let k = window.innerWidth/1440
        let m = window.innerHeight/800

        if(k>m) 
            k=m

        if(k!=kff){
            setKff((k>1)?1:k)
        }
    }

    const [kff, setKff] = useState(1);
    const [loaded, setLoaded] = useState(false);

    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    const [part, setPart] = useState(0);
    const [spart, setsPart] = useState(0);

    useEffect(()=>{
        gsap.killTweensOf([`.wtr-navigator-wave`]);
        gsap.set(`.wtr-navigator-wave[data-id="1"]`, {x:0});
        gsap.set(`.wtr-navigator-wave[data-id="0"]`, {x:0});
        gsap.to(`.wtr-navigator-wave[data-id="1"]`, {x:-67, repeat: -1, duration:1.5});
        gsap.to(`.wtr-navigator-wave[data-id="0"]`, {x:-67, repeat: -1, duration:1});

        gsap.killTweensOf([`.wtr-navigator-waves`]);
        gsap.set(`.wtr-navigator-waves`, {y:`${81 - 0*20}%`, duration:1});

        window.addEventListener('resize', sedda);
        sedda()
    }, [])


    return(
        <Water 
            mobile={mobile} 
            kff={kff} 
            loaded={loaded} 
            setLoaded={setLoaded} 
            part={part} 
            spart={spart} 
            setPart={setPart} 
            setsPart={setsPart} 
        />
    ) 
}
