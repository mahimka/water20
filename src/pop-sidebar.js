import React, { Component, useRef, useState, useEffect, useContext } from 'react'
import Sticky from '@ips/react/components/sticky'
import Text from './text'
import Drapirovka from  '@ips/react/components/unsafe-html'

import './pop-sidebar.styl'

const BooSvg = `<svg width="31" height="19" viewBox="0 0 31 19" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="31" height="3" fill="#FA6760"/>
<rect y="8" width="31" height="3" fill="#FA6760"/>
<rect y="16" width="31" height="3" fill="#FA6760"/>
</svg>
`

const BtnClose = p=>(<Drapirovka className="btn-close" onClick={p.onClick} content={BooSvg}/>)

const PopSidebar = p=>{
    return (<Sticky vh100 className="pop-sidebar">
                <Text mod="sidebar" text={`<span>••••</span><span>${p.name||''}<span>`}/>
                <BtnClose onClick={p.onClickClose}/>
                <Text mod="sidebar" text={`<span>Инновационная Москва #${(p.id||0)}</span><span>••••</span>`}/>
            </Sticky>)
}

export default PopSidebar