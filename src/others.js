import React, { Component, useRef, useState, useEffect, useContext } from 'react'
import { useMemo, useCallback } from 'use-memo-one'
import useTimeout from 'usetimeout-react-hook'

import Slider from './slider'

import * as __ from '@ips/app/hidash'
import Pic from '@ips/react/components/pic'
import Overlay from '@ips/react/components/overlay'
import Drapirovka from '@ips/react/components/unsafe-html'
import Text from './text'
import { SliceBox, SliceCon, Column, Row, useGridStyle } from '@ips/react/components/layout'
import cx from '@ips/app/classnamex'
import { useSizerSize } from '@ips/react/components/utils/use-sizer'

import { MobileCard, OthersSlide } from './article-blocks'
import { NaviAccord } from './navi-accord'

import './others.styl'

// const OthersSlide = p=>{
//     trace('cthumb', p.cthumb)
//     return useMemo(()=>(<Column className="others-slide" onClick={p.onClick}>
//                 <div className="kortina">
//                     <Pic mode="background" res2x src={p.cthumb||p.thumb} aspect="0.65333333"/>
//                 </div>
//                 <div className="infos">
//                     <Text textStyle="others-text" text={p.name} padding="10px 0 0 0"/>
//                     {/*<Text mod="odesc" text={p.desc}/>*/}
//                 </div>
//             </Column>),[p.cthumb||p.thumb, p.name, p.onClick])
// }


const svgArrow = `<svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M20.2557 1.68959L2.69141 19.2539L20.2557 36.8182" stroke="#8C7F50" stroke-width="3"/>
</svg>
`

const OthersMobileAccord = p=><NaviAccord persons={p.persons} mode="hover" onOpenPerson={p.onOpenPerson}/>


const Others = p=>{
    const [prelo, setPrelo] = useState(true)
    const { person={} } = p
    
    const gs = useGridStyle()
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    const slider = useRef()
    // useEffect(()=>{
    //     trace('got slider', slider.current)
    // })

    // const stopPrelo = useCallback(()=>setPrelo(false),[])
    // useTimeout(stopPrelo, 2000)
    useTimeout(()=>slider.current?slider.current.setSlideHeightAndWidth():null, 500)


    const [slides, slideIndex] = useMemo(()=>{
        const pps = p.persons.filter(p=>p.id != person.id)
        let cur = pps.findIndex(p=>p.id > person.id)
        if(!~cur) // -1
            cur = 0
        trace('slideIndex', cur, pps[cur].id, person.id)
        const C = mobile?MobileCard:OthersSlide
        return [
            pps.map((pp, i)=><C key={i} {...pp} onClick={()=>{
                            trace('ongon', pp); p.onOpenPerson('person', { person:pp.id })}}/>),
            cur]

    },[person.id, mobile])

    // if(prelo)
    //     return slides

    if(mobile)
        return <div className="others">
{/*                <Text textStyle="others-head" text="СЛЕДУЮЩИЙ ОБЪЕКТ"/>
                <Column width="10"  className={cx("divider", gs.gutterClass())} left="1"><div/></Column>
                <Column padding="15px 30px 0px 30px">
                    <Slider 
                        ref={slider}
                        heightMode="first" 
                        dragging={false} 
                        slidesToShow={1} 
                        speed={300} 
                        cellSpacing={0} 
                        slidesToScroll={1} 
                        wrapAround={false} 
                        withoutControls={true}
                        slideIndex={slideIndex}
                    >
                        {slides}
                    </Slider>
                </Column>
*/}
                <Column padding="0px 0 32px 0">
                    <Pic src="others-top-m.svg"/>
                </Column>

                <OthersMobileAccord {...p} excludePerson={person.id}/>
                {p.after}
            </div>

    return <SliceBox className="others">
                <Overlay cover backgroundColor="#E9DECD"/>
                <SliceCon  column padding="60px 0 100px 0">
                    {/*<Overlay cover mode="img" src="trinav.svg"/>*/}
                    <Column width="10" left="1" className="title" padding="0px 0 32px 0">
                        {/*<Text textStyle="others-head" text="ДРУГИЕ ОБЪЕКТЫ"/>*/}
                        <Pic src="others-top.png"/>
                    </Column>
                    <Column width="74%" left="13%">
                        <Slider 
                            ref={slider}
                            key={person.id}
                            heightMode="max" 
                            dragging={false} 
                            slidesToShow={3} 
                            speed={300} 
                            cellSpacing={60} 
                            slidesToScroll={1} 
                            wrapAround={true} 
                            withoutControls={false}
                            slideIndex={slideIndex}
                            initialSlideHeight={209}
                            renderBottomCenterControls={null}
                            renderCenterLeftControls={({ previousSlide }) => (
                                    <Drapirovka className="ctl-btn prev" onClick={previousSlide} content={svgArrow}/>
                            )}
                            renderCenterRightControls={({ nextSlide }) => (
                                    <Drapirovka className="ctl-btn next" onClick={nextSlide} content={svgArrow}/>
                            )}
                        >
                            {slides}
                        </Slider>
                    </Column>
                    {p.after}
                </SliceCon>
            </SliceBox>
}


export default Others
