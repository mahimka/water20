import React, { Component, useRef, useMemo, useState, useEffect, useContext } from 'react'
import { useRouter, useRoute } from 'react-router5'

import Anchor from '@ips/react/components/analytics-anchor'

import Overlay from '@ips/react/components/overlay'

import './popup.styl'

// import PopSidebar from './pop-sidebar'

import Article01 from './article-01'
import Article02 from './article-02'
import Article03 from './article-03'
import Article04 from './article-04'
import Article05 from './article-05'
import Article06 from './article-06'
import Article07 from './article-07'
import Article08 from './article-08'
import Article09 from './article-09'
import Article10 from './article-10'
// import Article11 from './article-11'
// import Article12 from './article-12'

const Articles = {
    1: Article01,
    2: Article02,
    3: Article03,
    4: Article04,
    5: Article05,
    6: Article06,
    7: Article07,
    8: Article08,
    9: Article09,
    10: Article10,
    // 11: Article11,
    // 12: Article12,
}

export default p=>{
    trace('Popup', p)
    const Cellar = p.cellar||'div'

    useEffect(()=>{
        if(p.onOpenDone)
            p.onOpenDone()
    })

    const Article = Articles[p.id]//||Article01
    trace('Article', p.id, Article)

    return  (<div className={`pop tpl-${p.tpl}`}>
                {/*<Overlay h100 className="sidebarcell">
                    <PopSidebar onClickClose={p.onClose} {...p}/>
                </Overlay>*/}
                <div className="contentcell">
                    <Anchor id={`anchor-${p.id}-1`}/>
                    <Overlay cover className="grback" mode="background" backgroundSize="auto" backgroundRepeat="repeat" src="bgsand.jpg"/>
                    <div className="articon">
                        <Article {...p}/>
                    </div>
                    <Anchor id={`anchor-${p.id}-2`}/>
                    <Cellar/>
                    <Overlay top="50px" right="32px" className="btn-close" mode="img" src="goback.png" onClick={p.onClose}></Overlay>
                </div>
            </div>)
}
