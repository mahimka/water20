import React from 'react'
import Text from '@ips/react/components/text'

// const DIText = p=><Text classPrefx="dinnov-" {...p}/>
const DIText = p=><Text {...p}/>

export default DIText
