import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import { useCallback, useMemo } from 'use-memo-one'

import ReactDOM from 'react-dom'

import { useRouter, useRoute } from 'react-router5'
import * as __ from '@ips/app/hidash'
import Pic from '@ips/react/components/pic'
// import ScreenHeight from '@ips/react/components/screen-height'
import Drapirovka from '@ips/react/components/unsafe-html'
import Overlay from '@ips/react/components/overlay'
import Vidos from '@ips/react/components/vidos'
import Sticky from '@ips/react/components/sticky'
import Vistracker from '@ips/react/components/vistracker'
import Slider from './slider'
import useTimeout from 'usetimeout-react-hook'

import { useSizerSize } from '@ips/react/components/utils/use-sizer'
// import useResize from '@ips/react/components/utils/use-resize'
import { Slice, SliceBox, SliceCon, Column, Row } from '@ips/react/components/layout'
import TextStyle from '@ips/react/components/text-style'
import { ListActivator } from '@ips/react/components/list-activator'
import { useActivation } from '@ips/react/components/activation'
import createEventTargetHook from '@ips/react/components/utils/create-event-target-hook'
import Text from './text'
import cx from '@ips/app/classnamex'

import { NaviAccord } from './navi-accord'

import './glagne.styl'

import { Card, MobileCard, BodyBlock, Lustra, InfogramHead, Infogram, TextButton, MobileOnly, DesktopOnly, Footer } from './article-blocks'


const Cover = p=>{
    const [ ssize ] = useSizerSize()
    const mobile = ssize===0

    return (<Column>        
                <SliceBox className="gla-cover">
                    {/*<Overlay cover mode="background" src="cover-back.jpg"/>*/}
                    <Overlay cover>
                        <Sticky>
                            <Column width="100%" minHeight="100vh">
                                <Vidos mode="background" srcId="cover" autoPlay loop muted/>
                            </Column>
                        </Sticky>
                    </Overlay>
                    <Overlay cover backgroundColor="rgba(0,0,0,0.7)" className="expr 'fader '+((event('titlevis', 'visible', 'visible')||'')?'':'vis') false"/>
                    <SliceCon align-self="center"  valign="space-between" column width="12" minHeight="screen" padding={mobile?"32px 0px 32px 0px":"32px 0px 32px 0px"}>
                            <Row left={mobile?"0":"4"} width={mobile?"100%":"4"} align="center"  className="cover-topcol">
                                <Pic noGutter src="cover-logo.svg" maxWidth="235px"/>
                            </Row>
                                <Pic src="cover-front.png" padding="64px 0px 0px 0px"/>
                            <Row width="100%" align="center"><Pic src="arr-down.svg" padding="16px 0px 128px 0px" noGutter maxWidth="32px"/></Row>
                    </SliceCon>                    
                    <SliceCon align-self="center" column width="12" minHeight={mobile?"70vh":"screen"} valign="center" padding={mobile?"32px 0px 32px 0px":"64px 0px 112px 0px"}>
                            <Column left="3" width="6">
                            <Vistracker name="titlevis">
                                <Text textStyle="main-lead" text="<p>Победа в Великой Отечественной войне на долгих четыре года стала общим делом для огромной страны. Каждый человек на фронте и в тылу точно знал: его труд крайне важен для слаженной работы гигантского механизма.</p>
                                <p>Руководители и рабочие московских предприятий не были исключением. В самое трудное и опасное время они отдавали все силы на то, чтобы обеспечить солдат всем необходимым. Десять историй московских предприятий в годы войны — в спецпроекте к 75-летию со дня Великой Победы.</p>"/>
                            </Vistracker>
                            </Column>
                    </SliceCon>     
                    <SliceCon>
                        {p.after}
                    </SliceCon>
                </SliceBox>
            </Column>)
}

const svgMapMarks = `<svg width="1080" height="970" viewBox="0 0 1080 970" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle id="mark_01" cx="167.5" cy="214.5" r="5.5" fill="#FF0000"/>
<circle id="mark_02" cx="798.5" cy="147.5" r="5.5" fill="#FF0000"/>
<circle id="mark_03" cx="504.5" cy="273.5" r="5.5" fill="#FF0000"/>
<circle id="mark_04" cx="674.5" cy="392.5" r="5.5" fill="#FF0000"/>
<circle id="mark_05" cx="619.5" cy="355.5" r="5.5" fill="#FF0000"/>
<circle id="mark_06" cx="904.5" cy="317.5" r="5.5" fill="#FF0000"/>
<circle id="mark_07" cx="872.5" cy="543.5" r="5.5" fill="#FF0000"/>
<circle id="mark_08" cx="419.5" cy="583.5" r="5.5" fill="#FF0000"/>
<circle id="mark_09" cx="486.5" cy="686.5" r="5.5" fill="#FF0000"/>
<circle id="mark_10" cx="933.5" cy="816.5" r="5.5" fill="#FF0000"/>
</svg>
`

const svgMarker = `<g>
<circle cx="26" cy="26" r="23" fill="#6A4815" stroke="#6A4815" stroke-width="6"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M38.6282 13.3716H34.7017V23.6197C34.6989 24.0569 34.138 24.3782 33.7594 24.1596L23.8155 18.4368L23.8057 23.0798C23.8057 23.5147 23.2536 23.8401 22.8732 23.6295L13.3711 18.3779V38.4324H18.7308L18.7504 32.2776C18.7497 31.8656 19.1253 31.5972 19.6535 31.6003H23.1775C23.5064 31.6003 23.8057 31.8996 23.8057 32.2286V38.6287H38.6282L38.6282 13.3716Z" fill="white"/>
</g>
`

const svgArrow = `<svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M20.2557 1.68959L2.69141 19.2539L20.2557 36.8182" stroke="#8C7F50" stroke-width="3"/>
</svg>
`

const PP = p=>{

    const active = useActivation()

    return (<Row 
        useGutter 
        valign="center" 
        className={cx("map-pp", active&&'active')} 
        onClick={p.onClick}
        onMouseEnter={p.onMouseEnter}
        onMouseLeave={p.onMouseLeave}>
        <Pic inline noGutter maxWidth="12%" src={p.icon}/>
        <Text textStyle="map-menu" text={p.name} padding="0px 0px 0px 16px"/>
    </Row>)
}

const Markers = p=>{
    return (<Overlay cover>
        </Overlay>)
}

const NaviMap = p=>{

    const { mode='click', onOpenPerson } = p

    const [coords, setCoords] = useState(null)
    const [activeItem, setActiveItem] = useState(-1)
    // trace('activeItem', activeItem)

    const markers = useRef()
    // const useMarkerEvent = useMemo(()=>createEventTargetHook(markers.current), [markers.current])
    const drops  = useCallback(()=>setActiveItem(-1), [])
    const mohs = useMemo(()=>__.map(p.persons, (pp,i)=>()=>setActiveItem(i)), [])
    const opens = useMemo(()=>__.map(p.persons, pp=>()=>onOpenPerson('person', { person: pp.id })), [])

    trace('markers.current', markers.current)

    useLayoutEffect(()=>{
        if(!markers.current) 
            return
        if(coords)
            return
        const coords = __.map(p.persons, (pp, i)=>{
            const T = markers.current.querySelector('#'+pp.marker)
            if(!T) return [0,0]
            const cx = T.getAttribute('cx')
            const cy = T.getAttribute('cy')
            return [cx, cy]
        })
        setCoords(coords)                
    },[markers.current])

    trace('have coords', coords)

    useLayoutEffect(()=>{
        trace('UE markers.current', markers.current)
        if(!coords || !markers.current) 
            return
        //  const NS = 'http://www.w3.org/2000/svg'
        // const G = document.createElementNS(NS, 'g')
        // G.setAttribute("transform", `translate(${coords[i][0]} ${coords[i][1]})`)
        // P.appendChild(G)

        const P = markers.current.querySelector('svg')
        
        ReactDOM.render(<Fragment>
            { __.map(p.persons, (pp, i)=>{

                const n = pp.shortName||pp.name

                return <g key={i} transform={`translate(${coords[i][0]} ${coords[i][1]})`}>
                    <g>
                    <foreignObject width="200" height="50" x="-85" y={(n.length > 15)?"-42":"-23"}>
                        <Text textStyle="map-marker" text={n}/>
                    </foreignObject>
                    </g>
                    <Drapirovka 
                        key={i}
                        component="g" 
                        className={cx('map-marker', activeItem==i&&'active')}
                        transform="translate(-12,0)"
                        content={svgMarker}
                        onClick={(mode == 'hover')?opens[i]:mohs[i]}
                        onMouseEnter={(mode == 'hover')?mohs[i]:null}
                        onMouseLeave={(mode == 'hover')?drops:null}
                        />
                </g>
            })}
            </Fragment>, P)

    })

    return (<SliceBox>
                <SliceCon padding="16px 0px">
                    {/*<Overlay cover backgroundColor="#F7F2EC"/>*/}
                    <Column width="3" valign="stretch">
                        <ListActivator naked current={activeItem}>
                            {__.map(p.persons, (pp, i)=><PP 
                                key={pp.id} 
                                onClick={(mode == 'hover')?opens[i]:mohs[i]}
                                onMouseEnter={mode=="hover"?mohs[i]:null} 
                                onMouseLeave={(mode == 'hover')?drops:null}
                                {...pp}/>)}
                        </ListActivator>
                    </Column>
                    <Column padding="0px 16px" width="9">
                        <Pic noGutter src="main-map.jpg"/>
                        <Overlay w100 top="1.5%">
                            <Column left="2" width="5" align="center">
                                <Text 
                                    textStyle="map-title" 
                                    style={{
                                        padding:'.7em 3.5em',
                                        borderRadius:'2em',
                                        backgroundColor:'white',
                                        maxWidth:"500px"
                                    }}
                                    text="Выберите метку на схеме и узнайте, как работало и что выпускало то или иное предприятие во время войны"/>
                            </Column>
                        </Overlay>
                        <Overlay cover>
                            <Drapirovka ref={markers} className="map-marks" content={svgMapMarks}/>
                            <ListActivator naked current={activeItem}>
                                {__.map(p.persons, pp=><Card pop key={pp.id} {...pp}/>)}
                            </ListActivator>
                        </Overlay>
                    </Column>
                </SliceCon>
            </SliceBox>)
}

const NaviSlider = p=>{
    const slider = useRef()
    useTimeout(()=>slider.current?slider.current.setSlideHeightAndWidth():null, 500)

    const [slides, slideIndex] = useMemo(()=>{
        // const pps = p.persons.filter(p=>p.id != person.id)
        // let cur = pps.findIndex(p=>p.id > person.id)
        // if(!~cur) // -1
        //     cur = 0
        // trace('slideIndex', cur, pps[cur].id, person.id)
        // const C = Card//mobile?Card:OthersSlide
        return [
            p.persons.map((pp, i)=><MobileCard key={i} {...pp} onClick={()=>{
                            trace('ongon', pp); p.onOpenPerson('person', { person:pp.id })}}/>),
            0]

    },[])

    return  <Column width="100%" padding="5vh 0">
                <Column width="85%" padding="0 0 1em 0px">
                    <Text textStyle="navislider-title" text="Листайте карточки, чтобы узнать о работе столичных предприятий во время войны"/>
                </Column>
                <Slider 
                    ref={slider}
                    // key={person.id}
                    heightMode="max" 
                    dragging={false} 
                    slidesToShow={1.2} 
                    speed={300} 
                    cellSpacing={20} 
                    slidesToScroll={1} 
                    wrapAround={true} 
                    withoutControls={false}
                    slideIndex={slideIndex}
                    initialSlideHeight={209}
                    renderBottomCenterControls={null}
                    renderCenterLeftControls={({ previousSlide }) => (
                    null
                    //        <Drapirovka className="ctl-btn prev" onClick={previousSlide} content={svgArrow}/>
                    )}
                    renderCenterRightControls={({ nextSlide }) => (
                    null

                    //        <Drapirovka className="ctl-btn next" onClick={nextSlide} content={svgArrow}/>
                    )}
                >
                    {slides}
                </Slider>
            </Column>
}





export default p=>{
    trace('Glagne', p)

    useEffect(()=>{
        if(p.onOpenDone)
            p.onOpenDone()
    })

    const [ ssize ] = useSizerSize()
    const mobile = ssize===0
    const commonStuff = useMemo(()=>(<Fragment></Fragment>),[])
    // after={mobile?<NaviSlider persons={p.persons} onOpenPerson={p.onOpenPerson}/>:null}
    
    return <div className="gla theme2">
                {/*
                <Overlay cover mode="background" backgroundSize="auto" backgroundRepeat="repeat" src="bgsand.jpg"/>
                {commonStuff}
                */}
                <Cover/>
                {/*
                {mobile ? 
                    <NaviAccord persons={p.persons} mode="hover" onOpenPerson={p.onOpenPerson}/> :
                    <NaviMap persons={p.persons} mode="hover" onOpenPerson={p.onOpenPerson}/> }
                <Footer/>
                */}
            </div>
}
