import React, { forwardRef } from 'react'

import Carousel from 'nuka-carousel'
trace('Carousel', Carousel)

import './slider.styl'
import * as __ from '@ips/app/hidash'

        // heightMode={null} 
        // initialSlideHeight={500} 
export default forwardRef((p, ref)=>
    (<Carousel 
        ref={ref} 
        {...p} 
        beforeSlide={(oldi,newi)=>{ if(newi!=oldi && p.onChange) p.onChange(newi) }}
        speed={p.speed||400}
        withoutControls={!__.ud(p.withoutControls)?p.withoutControls:true}
        renderBottomCenterControls={p.renderBottomCenterControls}
        renderCenterLeftControls={p.renderCenterLeftControls}
        renderCenterRightControls={p.renderCenterRightControls}
    />))
