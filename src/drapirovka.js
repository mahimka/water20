import React, { forwardRef } from 'react'

//className={props.className||''}
export default forwardRef((p, ref)=>{
    const {content='', ...otherProps} = p

    return (<div
        ref={ref}
        {...otherProps}
        dangerouslySetInnerHTML={{ __html:content }}
    />)
})