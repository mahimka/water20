import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'

import P1 from '../parts/part1'
import P2 from '../parts/part2'
import P3 from '../parts/part3'
import P4 from '../parts/part4'
 import P5 from '../parts/part5'

export default p=>{

    useEffect(()=>{
        p.setLoaded(true)
    }, [])


    return(
        <div className={`wtr-parts` + ((p.loaded)?' wtr-parts-loaded':'')}>
    
            <P1 part={p.part} spart={p.spart} autoplay={false} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded} />

            <P4 part={p.part} spart={p.spart} autoplay={false} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded} />
         
            <P3 part={p.part} spart={p.spart} autoplay={false} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded} />
    
            <P2 part={p.part} spart={p.spart} autoplay={false} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded} />

            
            <P5 part={p.part} spart={p.spart} autoplay={false} kff={p.kff} loaded={p.loaded} setLoaded={p.setLoaded} />
            
        </div>
    ) 
}
