import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'

import data from '../data/p4.json';


import tube3_4 from '../data/3_4.json';

import Lottie from 'react-lottie-segments';

export default p=>{

    let ff = true
    let ra = [0,1];
    let ra_3_4=[0, 1];
    if(p.part < 3){
        ra = [0,1];ra_3_4=[0, 1];
    } else if(p.part==3){
        if(p.spart==0) {ra = [0, 1]; ff = true;}
        if(p.spart==1) {ra = [1, 100]; ff = false;}
        if(p.spart==2) {ra = [100, 200]; ff = false}
        if(p.spart==3) {ra = [200, 300]; ff = false}
        if(p.spart==4) {ra = [300, 400]; ff = false}
        if(p.spart==5) {ra = [400, 528]; ff = true; ra_3_4=[1, 100];}
        if(p.spart>5) {ra=  [528, 529]; ff = false;ra_3_4=[99, 100];}
    }else if(p.part > 3){
        ra = [528,529];ra_3_4=[99, 100];
    }

    // console.log(`-------------`)
    // console.log(p.part, p.spart)
    // console.log('ra: ', ra)

    let sequence = { segments: ra, forceFlag: ff }
    let sequence_3_4 = { segments: ra_3_4, forceFlag: false }
    return(
        <div className={`wtr-part`} data-id={"4"}>

            <div className="wtr-scene">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: p.autoplay, 
                        animationData: data,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={data.w*0.9*p.kff}
                    height={data.h*0.9*p.kff}
                    isStopped={false}
                    isPaused={false}
                    eventListeners={[
                      {
                        eventName: 'enterFrame',
                        callback: (e) => console.log(e),
                      }
                    ]}
                    playSegments={sequence}
                />
            </div>

            <div className="wtr-tube" data-id="3_4">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: p.autoplay, 
                        animationData: tube3_4,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={tube3_4.w*1*p.kff}
                    height={tube3_4.h*1*p.kff}
                    isStopped={false}
                    isPaused={false}
                    playSegments={sequence_3_4}
                />
            </div>

        </div>
    ) 
}
