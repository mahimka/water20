import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'

import data from '../data/p2.json';
import tube from '../data/1_2.json';

import Lottie from 'react-lottie-segments';

export default p=>{

    let ra = [0,1]
    let ff = true
    if(p.part < 1){
        ra = [0,1]
    } else if(p.part==1){
        if(p.spart==0) {ra = [0, 1]; ff = true}
        if(p.spart==1) {ra = [1, 90]; ff = false}
        if(p.spart==2) {ra = [90, 91]; ff = false}
        if(p.spart==3) {ra = [91, 92]; ff = false}
        if(p.spart==4) {ra = [92, 170]; ff = false}
        if(p.spart==5) {ra = [170, 171]; ff = false}
        if(p.spart==6) {ra = [171, 290]; ff = false}
        if(p.spart==7) {ra = [290, 370]; ff = false}
        if(p.spart==8) {ra = [370, 574]; ff = false}
        if(p.spart>8) {ra=  [574, 575]; ff = false}
    }else if(p.part > 1){
        ra = [574,575]
    }

    // console.log(`-------------`)
    // console.log(p.part, p.spart)
    // console.log('ra: ', ra, 'ff: ', ff)

    let sequence = { segments: ra, forceFlag: ff }

    return(
        <div className={`wtr-part`} data-id={"2"}>
            

            <div className="wtr-tube" data-id="1_2">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: p.autoplay, 
                        animationData: tube,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={tube.w*0.9*p.kff}
                    height={tube.h*0.9*p.kff}
                    isStopped={false}
                    isPaused={false}

                />
            </div>


            <div className="wtr-scene">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: false, 
                        animationData: data,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={data.w*0.9*p.kff}
                    height={data.h*0.9*p.kff}
                    isStopped={false}
                    isPaused={false}
                    playSegments={sequence}
                />
            </div>



        </div>
    ) 
}
