import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'

import data from '../data/p5.json';

import Lottie from 'react-lottie';

export default p=>{
    return(
        <div className={`wtr-part`} data-id={"5"}>
            <Lottie 
                options={{
                    loop: true,
                    autoplay: p.autoplay, 
                    animationData: data,
                    rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                }}
                width={data.w*0.9*p.kff}
                height={data.h*0.9*p.kff}
                isStopped={false}
                isPaused={p.part!=4}
            />
        </div>
    ) 
}
