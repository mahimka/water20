import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'

import data from '../data/p3.json';
import tube2_3 from '../data/2_3.json';
import tube3_1 from '../data/3_1.json';

import Lottie from 'react-lottie-segments';

export default p=>{
    
    let ra = [0,1]; 
    let ra_2_3=[39, 40];
    let ra_3_1=[0, 1];
    let ff = true
    if(p.part < 2){
        ra = [0,1]; ra_2_3=[0, 1]; ra_3_1=[0, 1];
    } else if(p.part==2){
        if(p.spart==0) {ra = [0, 1]; ff = true; ra_2_3=[0, 1]}
        if(p.spart==1) {ra = [0, 1]; ff = false; ra_2_3=[1, 40]}
        if(p.spart==2) {ra = [1, 2]; ff = false}
        if(p.spart==3) {ra = [2, 96]; ff = false}
        if(p.spart==4) {ra = [97, 185]; ff = false}
        if(p.spart==5) {ra = [185, 186]; ff = false}
        if(p.spart==6) {ra = [187, 298]; ff = false}
        if(p.spart==7) {ra = [299, 340]; ff = false}
        if(p.spart==8) {ra = [341, 528]; ff = false; ra_3_1=[1, 130]}
        if(p.spart>8) {ra=  [528, 529]; ff = true; ra_3_1=[129, 130]}
    }else if(p.part > 2){
        ra = [528,529]; ra_2_3=[39, 40]; ra_3_1=[129, 130]
    }

    // console.log(`-------------`)
    // console.log(p.part, p.spart)
    // console.log('ra: ', ra)
    
    let sequence = { segments: ra, forceFlag: ff }
    let sequence_2_3 = { segments: ra_2_3, forceFlag: true }
    let sequence_3_1 = { segments: ra_3_1, forceFlag: true }

    return(
        <div className={`wtr-part`} data-id={"3"}>
            
            <div className="wtr-tube" data-id="2_3">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: p.autoplay, 
                        animationData: tube2_3,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={tube2_3.w*1*p.kff}
                    height={tube2_3.h*1*p.kff}
                    isStopped={false}
                    isPaused={false}
                    playSegments={sequence_2_3}
                />
            </div>

            <div className="wtr-scene">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: p.autoplay, 
                        animationData: data,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={data.w*0.9*p.kff}
                    height={data.h*0.9*p.kff}

                    isStopped={false}
                    isPaused={false}
                    playSegments={sequence}

                />
            </div>

            <div className="wtr-tube" data-id="3_1">
                <Lottie 
                    options={{
                        loop: false,
                        autoplay: p.autoplay, 
                        animationData: tube3_1,
                        rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                    }}
                    width={tube3_1.w*1*p.kff}
                    height={tube3_1.h*1*p.kff}
                    isStopped={false}
                    isPaused={false}
                    playSegments={sequence_3_1}
                />
            </div>

        </div>
    ) 
}
