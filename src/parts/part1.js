import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import ReactDOM from 'react-dom'

import data from '../data/p1.json';

import Lottie from 'react-lottie-segments';

export default p=>{
    return(
        <div className={`wtr-part`} data-id={"1"}>
            <Lottie 
                options={{
                    loop: true,
                    autoplay: p.autoplay, 
                    animationData: data,
                    rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                }}
                width={data.w*0.9*p.kff}
                height={data.h*0.9*p.kff}
                isStopped={false}
                isPaused={p.part!=0}
            />
        </div>
    ) 
}
