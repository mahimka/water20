import React, { Component, Fragment, useRef, useState, useEffect, useLayoutEffect, useContext } from 'react'
import { useCallback, useMemo } from 'use-memo-one'
import * as __ from '@ips/app/hidash'
import cx from '@ips/app/classnamex'
import { Column, Row } from '@ips/react/components/layout'
import Pic from '@ips/react/components/pic'
import Vidos from '@ips/react/components/vidos'
import Text from './text'
import { TextButton } from './article-blocks'

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemState,
    AccordionItemPanel,
} from 'react-accessible-accordion'
// import 'react-accessible-accordion/dist/fancy-example.css'


// const AccordItem = p=>(<Row className="accord-item" valign="center" padding="1.5em 20px">
//                         <Pic noGutter src={p.icon} maxWidth="7%"/>
//                         <Text textStyle="accord-item" text={ p.shortName }/>
//                     </Row>)

// const AccordCard = p=>(<Column className="accord-card">
//                 <Row valign="center" padding="0px 20px">
//                     <Pic noGutter src={p.icon} maxWidth="4%"/>
//                     <Text textStyle="pop-arms" text={p.arms} padding=".5em 0 .5em .5em"/>
//                 </Row>
//                 <Text textStyle="pop-name" text={ p.name } padding=".5em 20px"/>
//                 <Text textStyle="pop-location" text={ p.location } padding="0 20px .5em 20px"/>
//                 <Column padding=".5em 20px" >
//                     <Vidos srcId={p.covert?p.covert:(p.cover+'t')} autoPlay loop muted/>
//                 </Column>
//                 <Text textStyle="pop-desc" text={ p.desc } padding=".5em 20px"/>
//                 <Row left="15%" width="70%" valign="center" padding="2em 0 3em 0">
//                     <TextButton mod="forward" text="СМОТРЕТЬ" onClick={p.onClick}/>
//                 </Row>
//             </Column>)

const AccordItem = p=>(<Column className={cx("accord-item", p.state.expanded&&'expanded')} valign="center" padding="1em 20px">
                            <Row valign="center">
                                <Pic noGutter src={p.icon} maxWidth="4%"/>
                                <Text textStyle="pop-arms" text={p.arms} padding=".5em 0 .5em .5em"/>
                            </Row>
                            <Text textStyle="pop-name" text={ p.name } padding=".5em 0px 0em 0"/>
                    </Column>)

const AccordCard = p=>(<Column className="accord-card">
                <Text textStyle="pop-location" text={ p.location } padding="0 20px .5em 20px"/>
                <Column padding=".5em 20px" >
                    <Vidos srcId={p.covert?p.covert:(p.cover+'t')} autoPlay loop muted/>
                </Column>
                <Text textStyle="pop-desc-mob" text={ p.desc } padding=".5em 20px"/>
                <Row left="15%" width="70%" valign="center" padding="2em 0 3em 0">
                    <TextButton mod="forward" text="СМОТРЕТЬ" onClick={p.onClick}/>
                </Row>
            </Column>)

export const NaviAccord = p=>{
    const items = useMemo(()=>(p.persons||[]).filter(pp=>pp.id != p.excludePerson).map(pp=>(
        <AccordionItem key={pp.id}>
            <AccordionItemHeading>
                <AccordionItemButton>
                    <AccordionItemState>
                        { state => <AccordItem {...pp} state={state}/> }
                    </AccordionItemState>
                    
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
                <AccordCard {...pp} onClick={()=>p.onOpenPerson('person', { person:pp.id })}/>
            </AccordionItemPanel>
        </AccordionItem>)
    ),[p.excludePerson])

    return  <Column key={p.excludePerson} width="100%" padding="1em 0em">
                <Accordion allowZeroExpanded >
                    { items }
                </Accordion>
            </Column>
}
