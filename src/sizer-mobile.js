const sedda = ()=>{
    // let vh = window.outerHeight * 0.01;
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    // document.documentElement.style.setProperty('--h', `${window.innerHeight}px`);
}

sedda()
// We listen to the resize event
window.addEventListener('resize', sedda);
