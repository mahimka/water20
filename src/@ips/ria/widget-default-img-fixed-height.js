import widgetInject from './widget-inject'

widgetInject((container, opts)=>{
    var cci = document.createElement('div')
    cci.style.width = '100%'
    cci.style.height = '100%'
    cci.style.paddingBottom='50%'
    cci.style.backgroundImage = 'url(' + opts.img + ')'
    cci.style.backgroundSize = 'contain'
    cci.style.backgroundPosition = 'center'
    cci.style.backgroundRepeat = 'no-repeat'

    var cca = document.createElement('a')
    cca.href = opts.url
    cca.target= "_blank"
    cca.appendChild(cci)

    var w = cca
    container.appendChild(w)
})
