import './topbar.styl'

function riaFix(){
    for(let i=0; i<document.styleSheets.length;i++){
        if (document.styleSheets.item(i).href) {
            if(document.styleSheets.item(i).href.indexOf('common') !== -1) {
                document.styleSheets.item(i).disabled=true
            }
        }
    }

    var is_safari = ( navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") == -1);
    if(is_safari) {
      var barShare = document.querySelector('.b-top-bar__share');
      if(barShare){
        barShare.style.float = 'none';
        setTimeout(function(){
          barShare.style.float = 'right';
        }, 0);
      }
    }    

    const topBar = document.querySelector('.b-top-bar-fixed')
    if(topBar){
        topBar.style.position = 'static'
    }
    const loader = document.getElementById('modalLayer')
    if(loader)
        loader.style.display = 'none'
}

if(window.__ips_embed_ria__){
    riaFix()
}
