export default function(render){

    function getParamsFromUrl(url) {
        if(!url) url = location.search;
        var query = url.substr(1);
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        return result;
    }

    // export const parseUrlQueryParams = s => _.map(s.substr(1).split('&'), p=>p.split('='))

    var currentScript
       = document.currentScript || document.scripts[document.scripts.length-1]

    var alls = document.querySelectorAll('script[data-uid]')
    console.log('all scripts with data-uid', alls)
    for(var i = 0; i < alls.length; i++){
        const s = alls[i]
        if(s.src.includes('ria.ru/ips/')){
            if(!s.getAttribute('data-ips-bound')){
                currentScript = s
                break
            }
            console.log(alls[i])
        }
    }
    currentScript.setAttribute('data-ips-bound', true)
    
    console.log('currentScript', currentScript)
    console.log('all scripts with attr data-uid', document.querySelectorAll('script[data-uid]'))
    console.log('currentScript data-uid', currentScript.getAttribute('data-uid'))
    console.log('url uid', getParamsFromUrl()['data-uid'])

    var uid = currentScript.getAttribute('data-uid')||getParamsFromUrl()['uid']

    var publicPath = (function(){
        if(currentScript)
            return currentScript.src.split('/').slice(0, -1).join('/')

        return location.origin + location.pathname.replace(/\/$/, '') // remove trailing slash
    })()

    const metanp = (p, d)=> {
        var el = (d||document).querySelector(`meta[name='${ p }']`)
        if(!el)
            el = (d||document).querySelector(`meta[property='${ p }']`)
        return el && el.getAttribute('content')
    }

    function makeMetas(s){
        var el = document.createElement('div')
        el.innerHTML = s

        return el
    }

    render = render || function(container, opts){
        const a = document.createElement('a')
        a.href = opts.url
        a.target = '_blank'
        a.innerText = opts.title
    }

    fetch([publicPath, 'index.html'].join('/'))
    .then(function(res){ return res.text() })
    .then(function(res){
        var index = makeMetas(res)

        const opts = {
            title: metanp('og:title', index) || (index.querySelector('title')||{}).innerText || publicPath,
            desc: metanp('og:description', index) || metanp('description', index) || '',
            img: metanp('ria:image', index) || metanp('og:image', index) || metanp('twitter:image', index),
            url:  metanp('ria:url', index) || metanp('og:url', index) || metanp('twitter:url', index) || publicPath,
        }

        var mountPoint = currentScript.parentElement
        console.log('mounting widget', opts, 'at', uid, mountPoint)
        render(mountPoint, opts)
    })
}
