//import { windowSize } from '@ips/app/dom-utils'
require('intersection-observer');

function destroy(){
    this.observer.disconnect()//.unobserve(this.el)
    this.observer = null;
}

export default function(el, cb = ()=>{}, cbInvis){
    trace('called trackit', el)
    if(!el.tagName){
        warn('vistracker utils: object doesn\'t seem to be a HTMLElement', el)
        return
    }

    var state = {
        el,
        cb,
        cbInvis,
        visible:false
    }

    let options = {
        root: null,
        //rootMargin: `${ 0 }px 0px ${ -700 }px 0px`,//(-300)+'px',//
        // threshold: .5//buildThresholdList()
    };

    //trace('opts', options)

    const handleIntersect = (inters)=>{
        // console.log('inters', this, self, inters)

        const newvis = inters[0].intersectionRatio > 0
        if(!state.visible){
            if(newvis){
                state.visible = true
                cb(true)
            }
        }else{
            if(!newvis){
                state.visible = false
                if(!cbInvis)
                    cb(false)
                else
                    cbInvis()
            }
        }
    }

    state.observer = new IntersectionObserver(handleIntersect, options);
    state.observer.observe(el);

    state.destroy = destroy.bind(state)

    return state
}
