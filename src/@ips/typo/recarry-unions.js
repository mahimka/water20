import _ from 'lodash'
var carryUnions = require('carry-unions');

module.exports = function reCarryUnions(elt){
    _.each(elt.childNodes, (d)=>{ 
        if (d.nodeType == 3){
            d.textContent = carryUnions(d.textContent);
        }
        else
            reCarryUnions(d);
    })
}
