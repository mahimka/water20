var jsx = require('./react-jsx')

console.log("Snarb")

const initJsxTranspiler = conn =>{
    // trace('initJsxTranspiler')
	conn.on("text", function (str) {
		const o = JSON.parse(str)
		// console.log("parsed ", o)
		try{
			// console.log("Received ", str)
			o.data = jsx.client(o.data, { ecma: 'es5' })
			// console.log("compiled ", res)
			conn.sendText(JSON.stringify(o))
		}catch(err){
			console.log('Error compiling JSX:', err)
		}
	})
	conn.on("close", function (code, reason) {
		console.log("Connection closed")
	})
	conn.on("error", function (error) {
		console.log(error)
	})
}

module.exports = initJsxTranspiler