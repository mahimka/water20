//  'use strict';

const path = require("path");

const ROOT = path.resolve(__dirname, '..')
const NODE_MODULES = path.resolve(__dirname, '../node_modules')
const IPS_REACT_COMPONENTS = path.join(NODE_MODULES, '@ips/react/components')
const IPS_APP = path.join(NODE_MODULES, '@ips/app')
const ELEMENT_REACT = path.join(NODE_MODULES, 'element-react')
const ELEMENT_REACT_THEME = path.join(NODE_MODULES, 'element-theme-default')


// Export a function. Accept the base config as the only param.
module.exports = (storybookBase, configType) => {

  // console.log('storybookBaseConfig', storybookBaseConfig)
  // console.log('storybookBaseConfig.module', storybookBaseConfig.module)

  storybookBase.config.module.rules.push(
      {
        test: /\.styl$/,
        loaders: ["style-loader", "css-loader", {
          loader:"stylus-loader",
          options:{
            preferPathResolver: 'webpack',
          }
        }],
        include: [ROOT, IPS_REACT_COMPONENTS]
      }
  )

  // console.log('storybookBase.config.module.rules', storybookBase.config.module.rules)

  // storybookBase.config.module.rules.push(
  //     {
  //       test: /\.css$/,
  //       loaders: ["style-loader", "css-loader"],
  //       include: [ROOT, IPS_REACT_COMPONENTS, ELEMENT_REACT, ELEMENT_REACT_THEME]
  //     }
  // )

  // storybookBaseConfig.module.rules.push(
  //     {
  //       test: /\.css$/,
  //       loaders: ["css-loader", "stylus-loader"],
  //       include: [ROOT, REACT_COMPONENTS]
  //     }
  // )

  var babelPlugins = [
    path.resolve(NODE_MODULES, '@babel/plugin-syntax-dynamic-import'),
    path.resolve(NODE_MODULES,'@babel/plugin-transform-async-to-generator'),
    path.resolve(NODE_MODULES,'@babel/plugin-transform-react-jsx'),
    // path.resolve(NODE_MODULES, 'babel-plugin-lodash'),
    path.resolve(NODE_MODULES, '@babel/plugin-proposal-object-rest-spread'),
    path.resolve(NODE_MODULES, '@babel/plugin-proposal-class-properties'),
    // path.resolve(NODE_MODULES, 'babel-plugin-react-css-modules')
  ]

  storybookBase.config.module.rules.push(
      {
          test: /.(js|jsx?)$/,
          loader: 'babel-loader',
          options: {
              plugins:babelPlugins,
              presets: [require.resolve('@babel/preset-env'), 
              //require.resolve('babel-preset-stage-0'), 
              require.resolve('@babel/preset-react')],
              // plugins: [require.resolve('babel-plugin-macros'), require.resolve('babel-plugin-transform-regenerator'), [require.resolve('babel-plugin-transform-runtime'), {
              //   helpers: true,
              //   polyfill: true,
              //   regenerator: true
              // }]]              
          },
          include: [IPS_REACT_COMPONENTS, IPS_APP]
      }
  )

  storybookBase.config.resolve.symlinks = false

  // Return the altered config
  return storybookBase.config;
};