import React from 'react';

// import "babel-polyfill"
import '@ips/app/trace'
import '@ips/app/normalize.css.styl'
import '@ips/app/modernizr'
import '@ips/app/sizer'

require('./components')
require('./moves')
require('./solutions')
require('./fx')
require('./test')
// const req = require.context('.', false, /.stories.js$/);
// req.keys().forEach(filename => req(filename))

