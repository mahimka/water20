import React, { Component } from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';

import { windowSize } from '@ips/app/dom-utils'

import Slice from   '@ips/react/components/slice';

import * as PIXI from 'pixi.js'

import './pixi.stories.styl'

const ppath = s =>s //`${ app.publicPath }/b.png`



class PixiTest extends Component{
    render(){
        return <div className='pixi-test' ref={ ref=>this.$el = ref }>
        </div>
    }

    componentDidMount(){
        const pixiApp = new PIXI.Application()
        this.pixiApp = pixiApp
        this.$el.appendChild(pixiApp.view)
        trace('got pixi', PIXI, pixiApp)

        try{
            this.props.init(pixiApp)
        }catch(err){
            error(err)
        }

        this.onResize()
        window.addEventListener('resize', this.onResize)
    }

    onResize = _.throttle(()=>{
        // const ws = windowSize()
        // this.pixiApp.renderer.resize(ws.x, ws.y)

        const s = [this.$el.offsetWidth, this.$el.offsetWidth]
        if(this.$el)
            this.pixiApp.renderer.resize(s[0], s[1])// make a square //this.$el.offsetHeight)

        if(this.pixiApp.onResize)
            this.pixiApp.onResize(s[0], s[1])
    }, 200)
    
}

storiesOf('Fx', module)
  .addWithJSX('Pixi - Flag', () => {


    const initFlag = pixiApp => {

        const loader = new PIXI.loaders.Loader()
        loader
            .add('back', ppath('b.png'))
            .add('noise', ppath('noiseb.png'))
            .load((loader, resources) => {

                const back = new PIXI.Sprite(resources.back.texture)
                const noise = new PIXI.Sprite(resources.noise.texture)

                back.scale = new PIXI.Point(.7,.7)
                noise.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT
                noise.scale = new PIXI.Point(30,30)
                const speed = 10

                // Add the bunny to the scene we are building
                pixiApp.stage.addChild(back);
                pixiApp.stage.addChild(noise);

                const disp = new PIXI.filters.DisplacementFilter(noise, 300)
                pixiApp.stage.filters = [disp]

                // Listen for frame updates
                pixiApp.ticker.add(() => {
                    noise.x += speed;
                    noise.y += speed;
                });

                pixiApp.onResize = (w, h)=>{
                    back.width = w
                    back.height = h
                }

            });    
    }
  
  	return <div>
                <Slice>
                    <PixiTest init={ initFlag }/>
                    <div id="mp"/>
                </Slice>
            </div>
  }, {
    // info:infoStyle,
  })
  .addWithJSX('Pixi - Water', () => {

    const initWater = pixiApp => {

        const loader = new PIXI.loaders.Loader()
        loader
            .add('back', ppath('b.png'))
            .add('noise', ppath('noise.png'))
            .load((loader, resources) => {

                const back = new PIXI.Sprite(resources.back.texture)
                const noise = new PIXI.Sprite(resources.noise.texture)

                back.scale = new PIXI.Point(.7,.7)
                noise.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT
                noise.scale = new PIXI.Point(10,10)
                const speed = 3

                // Add the bunny to the scene we are building
                pixiApp.stage.addChild(back);
                pixiApp.stage.addChild(noise);

                const disp = new PIXI.filters.DisplacementFilter(noise, 150)
                pixiApp.stage.filters = [disp]

                // Listen for frame updates
                pixiApp.ticker.add(() => {
                    noise.x += speed;
                    noise.y += speed;
                });

                pixiApp.onResize = (w, h)=>{
                    back.width = w
                    back.height = h
                }
            });    
    }

    return <div>
                <Slice>
                    <PixiTest init={ initWater }/>
                    <div id="mp"/>
                </Slice>
            </div>
  }, {
    // info:infoStyle,
  })
