import React from 'react';

import { storiesOf } from '@storybook/react';
//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

import Deepegg from   './deepegg';

const addFn = 'addWithJSX'

storiesOf('Test', module)
  .addWithJSX('Deepegg', () => (
    <Deepegg cans={ 10 } s={ 500 }/>
  ))
