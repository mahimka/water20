import React, { Component } from 'react';

import { storiesOf } from '@storybook/react';

import { request } from  '@ips/app/app-registry'

import { loadScript } from '@ips/app/utils'
import DragTracker from '@ips/react/components/drag-tracker'

//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

const addFn = 'addWithJSX'



class SketchfabTest extends Component{
    render(){
        return <div className='sketchfab-test' ref={ ref=>this.$el = ref }>
            <iframe width="640" height="360" src="" id="api-frame" allow="autoplay; fullscreen; vr" allowvr allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
            <DragTracker name="dt1" range="0, 1000" loop>
            THIS IS A CONTROLLER. DRAG OVER IT TO ROTATE THE MODEL
            </DragTracker>

        </div>
    }

    componentDidMount(){

        const { modelId } = this.props

        loadScript('https://static.sketchfab.com/api/sketchfab-viewer-1.5.0.js').then(()=>{

            var iframe = document.getElementById( 'api-frame' );
            // var uid = '7w7pAfrCfjovwykkEeRFLGw5SXS';

            // By default, the latest version of the viewer API will be used.
            var client = new Sketchfab( iframe );

            // Alternatively, you can request a specific version.
            // var client = new Sketchfab( '1.5.0', iframe );

            client.init( modelId, {
                success: function onSuccess( api ){
                    api.start();
                    api.addEventListener( 'viewerready', function() {

                        // API is ready to use
                        // Insert your code here
                        console.log( 'Viewer is ready' );

                    } );

                    request(['dt1']).then( res =>{
                        trace('got dt1', res.dt1)
                        res.dt1.ee.on('drag', _.throttle(e=>{
                            trace('dt update', e)

                            const ang = 3.14159265357/180*e.position

                            api.lookat([Math.sin(ang)*2, Math.cos(ang)*2, 0.6], [0,0,0.3], 0)
                        }, 0))
                    })                    
                },
                error: function onError() {
                    console.log( 'Viewer error' );
                }
            } ); 



        })
    }

}


storiesOf('Test', module)
  .addWithJSX('Sketchfab + DragTracker', () => (
    <SketchfabTest modelId='17701831d4cb4383b5cf0306806adbea'/>
  ))
