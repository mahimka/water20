import React, { Component } from 'react'

import paper from 'paper'

import './deepegg.styl'

trace('paper', paper)

const kona = (i, s) => <div><canvas key={ i } width={ s } height={ s }></canvas></div>

export default class Deepegg extends Component{
    // props = {
    //     cans:10,
    //     s:500,
    // }

    componentDidMount(){
        const { s, cans } = this.props

        let base = -10
        _.times(cans, i => {
            const $lc = this.$lcans.children[i].querySelector('canvas')
            const lc = new paper.Project($lc)

            lc.activate()
            var point = new paper.Point(0, 0)
            var size = new paper.Size(s, s)

            var x = i/cans
            var path = new paper.Path.Rectangle(point, size).subtract(new paper.Path.Circle(s/2, s/2, s*0.3*Math.sqrt(1-x*x)))
            const c = 255/(1 + x*x)-100
            path.fillColor = `rgb(${ c } ${ c } ${ c })`
            lc.view.draw()
        })

        _.times(cans, i => {
            const $lc = this.$lcans.children[i].querySelector('canvas')
            $lc.style.transform = `translate3d(0px,0px, ${ base - i }px)`
        })

        // base = 0
        _.times(cans, i => {
            const $rc = this.$rcans.children[i].querySelector('canvas')
            const rc = new paper.Project($rc)

            rc.activate()

            var x = i/cans
            const ix = 1-x
            if(!i){

                var point = new paper.Point(0, 0)
                var size = new paper.Size(s, s)

                var rect = new paper.Path.Rectangle(point, size)
                const cix = 1-x
                const c = 255/(1 + cix*cix)+27
                rect.fillColor =  `rgb(${ c } ${ c } ${ c })`
            }
            var path = new paper.Path.Circle(s/2, s/2, s*0.3*Math.sqrt(1-x*x))
            const c = 255/(1 + ix*ix)
            path.fillColor = `rgb(${ c } ${ c } ${ c })`
            rc.view.draw()
        })
        _.times(cans, i => {
            const $rc = this.$rcans.children[i].querySelector('canvas')
            $rc.style.transform = `translate3d(0px,0px, ${ base + i }px)`
        })

        // paper.setup(this.$el)
        // var path = new paper.Path();
        // // Give the stroke a color
        // path.strokeColor = 'black';
        // var start = new paper.Point(100, 100);
        // // Move to start and draw a line from there
        // path.moveTo(start);
        // // Note that the plus operator on Point objects does not work
        // // in JavaScript. Instead, we need to call the add() function:
        // path.lineTo(start.add([ 200, -50 ]));
        // // Draw the view now:
        // paper.view.draw();        

        trace('we', this)

        window.addEventListener('mousemove', e =>{
            trace('mm', e)
            const rx = e.clientY - window.innerHeight/2
            const ry = e.clientX - window.innerWidth/2
            if(!this.$stage) return
            trace('hasta')
            this.$stage.style.transform = ` rotateX(${ rx*0.01 }deg) rotateY(${ ry*0.01 }deg) translate3d(${ e.clientX }px, ${ e.clientY  }px, 0px)`
            // this.$stage.style.transform = `rotateX(${ rx*0.01 }deg) rotateY(${ ry*0.01 }deg) translate3d(${ window.innerWidth/2 }px, ${ window.innerHeight/2  }px, 0px)`

        })
    }

    render(){
        const { cans, s } = this.props

        // <canvas ref={ ref => this.$el = ref }></canvas></div>

        return <div className="deepegg">
                    <div className="stage" ref={ ref => this.$stage = ref }>
                        <div className="lcans" ref={ ref => this.$lcans = ref }>
                            { _.times(cans, i => kona(i, s)) }
                        </div>
                        <div className="rcans" ref={ ref => this.$rcans = ref }>
                            { _.times(cans, i => kona(i, s)) }
                        </div>                    
                    </div>
                </div>
    }
}
