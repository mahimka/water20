import React, { Component } from 'react';

import { storiesOf } from '@storybook/react';

import { request } from  '@ips/app/app-registry'

import { loadScript } from '@ips/app/utils'
// import DragTracker from '@ips/react/components/drag-tracker'
import Slice from '@ips/react/components/slice'

import '@google/model-viewer'

import './google-model-viewer.stories.styl'

//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

const addFn = 'addWithJSX'


class ModelViewerTest extends Component{

    state = {}

    render(){
        const { mvLoaded } = this.state

        return <div className='google-model-viewer-test' ref={ ref=>this.$el = ref }>
            <Slice><h2>Check out documentation <a href="https://googlewebcomponents.github.io/model-viewer/index.html" target="_blank">here</a></h2></Slice>
            { mvLoaded ? <model-viewer ref={ ref=>this.$modelViewer = ref } src="notredame_16.glb" auto-rotate camera-controls></model-viewer> : 'Loading viewer...' }
            {/*<DragTracker name="dt1" range="0, 1000" loop>
            THIS IS A CONTROLLER. DRAG OVER IT TO ROTATE THE MODEL
            </DragTracker>*/}

        </div>
    }

    componentWillMount(){
        loadScript('https://dc.ria.ru/ips/lib/model-viewer.js').then(()=>{
            this.setState({ mvLoaded:true })
        })
    }

    componentDidMount(){

    //                 request(['dt1']).then( res =>{
    //                     trace('got dt1', res.dt1)
    //                     res.dt1.ee.on('drag', _.throttle(e=>{
    //                         trace('dt update', e)

    //                         const ang = 3.14159265357/180*e.position

    //                         api.lookat([Math.sin(ang)*2, Math.cos(ang)*2, 0.6], [0,0,0.3], 0)
    //                     }, 0))
    //                 })                    
    }

    componentDidUpdate(){
        trace('this.$modelViewer', this.$modelViewer)

        const orbitCycle = [
            '45deg 55deg 2m',
            '-60deg 110deg 1m',
            this.$modelViewer.cameraOrbit
          ];

          setInterval(() => {
            const currentOrbitIndex = orbitCycle.indexOf(this.$modelViewer.cameraOrbit);
            this.$modelViewer.cameraOrbit =
                orbitCycle[(currentOrbitIndex + 1) % orbitCycle.length];
          }, 3000);        
    }


}


storiesOf('Test', module)
  .addWithJSX('Google ModelViewer', () => (
    <ModelViewerTest modelId='17701831d4cb4383b5cf0306806adbea' 
/>
  ))
