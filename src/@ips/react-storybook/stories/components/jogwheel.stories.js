import React from 'react';

import { storiesOf } from '@storybook/react';
//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

import Fixed from '@ips/react/components/fixed'
import ScrollTrackerC from '@ips/react/components/scroll-tracker'
const ScrollTracker = ScrollTrackerC.ScrollTrackerF

import DragTracker from '@ips/react/components/drag-tracker'
import DText from '@ips/react/components/dtext'
import Jogwheel from   '@ips/react/components/jogwheel';

import './jogwheel.stories.styl'

const addFn = 'addWithJSX'
// name="jwt" duration={ 1 }>
storiesOf('Components/Jogwheel', module)
  .addWithJSX('(default)', () => (
    <Jogwheel progress={ .1} className="jw-test"> 
        <div className="jw-test">jw-test</div>
    </Jogwheel>
  ))
  .addWithJSX('perf test', () => (
    <div>
        <Fixed cover>
{/*            <Context name="qqq" expr="eve=event('jwst', 'update', 'pos');eve1=(eve - .1)||0"/>
            <Context>
                <Var name="eve" value="expr event('jwst', 'update', 'pos')"/>
                <Var name="eve1" value="expr (eve - .1)||0"/>
                <Var name="z" value="expr lerp([[0, 0.3], [0.3, 0.1], [0.5, 0.7]], event('jwst', 'update', 'pos'))"/>
            </Context>*/}
            {/*<Way continous/>*/}
            <span>Progress:</span>
            <DText text="expr event('jwst', 'update', 'pos')"/>
            <Jogwheel progress="expr (event('jwst', 'update', 'pos') - 0.1)||0" className="jw-test"> 
                <div>jw-test</div>
            </Jogwheel>
            <Jogwheel progress="expr (event('jwst', 'update', 'pos') - 0.2)||0"  className="jw-test"> 
                <div>jw-test</div>
            </Jogwheel>
            <Jogwheel progress="expr (event('jwst', 'update', 'pos') - 0.3)||0"  className="jw-test"> 
                <div>jw-test</div>
            </Jogwheel>
            <Jogwheel progress="expr (event('jwst', 'update', 'pos') - 0.4)||0"  className="jw-test"> 
                <div>jw-test</div>
            </Jogwheel>
            <Jogwheel progress="expr (event('jwst', 'update', 'pos') - 0.5)||0"  className="jw-test"> 
                <div>jw-test</div>
            </Jogwheel>
        </Fixed>
        <div style={{ height: '200vh' }}/>
        <ScrollTracker name="jwst" throttle="0">
            <div className="st" style={{ height: '300vh' }}/>
        </ScrollTracker>
        <div style={{ height: '200vh' }}/>
    </div>
  ))

storiesOf('Moves', module)
  .addWithJSX('Jogwheel + ScrollTracker', () => (
    <div>
        <Fixed cover>
            <span>Progress:</span>
            <DText text="expr event('jwst', 'update', 'pos')"/>
            <Jogwheel progress="expr event('jwst', 'update', 'pos')" className="jw-test"> 
                <div>jw-test</div>
            </Jogwheel>
        </Fixed>
        <div style={{ height: '200vh' }}/>
        <ScrollTracker name="jwst" throttle="0">
            <div className="st" style={{ height: '300vh' }}/>
        </ScrollTracker>
        <div style={{ height: '200vh' }}/>
    </div>
  ))

storiesOf('Moves', module)
  .addWithJSX('Jogwheel + DragTracker', () => (
    <div>
        <Fixed cover>
            <span>Progress:</span>
            <DText text="expr event('jwst', 'update', 'pos')"/>
            <DragTracker name="dt1" range="0, 1000" loop>
                <Jogwheel progress="expr 1-(event('dt1', 'drag', 'position')|0)/1000" className="jw-test"> 
                    <div>jw-test</div>
                </Jogwheel>
            </DragTracker>
        </Fixed>
    </div>
  ))
