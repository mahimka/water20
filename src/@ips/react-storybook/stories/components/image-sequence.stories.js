import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';


import Readme from './image-sequence.md';

import ImageSequence from '@ips/react/components/image-sequence';

const addFn = 'addWithJSX'


storiesOf('Components/ImageSequence', module)
  .add('Doc', doc(Readme))
  //.add('Info', withReadme(Readme, ()=><div dangerouslySetInnerHTML={{ __html:info }}/>))
  // .addDecorator(withInfo)


storiesOf('Components/ImageSequence', module)
  [addFn]('(default)', () => (
    <ImageSequence images={ threesixty } />
  ), {
    // info:infoStyle,
  })


const threesixty = "threesixty_1.jpg,threesixty_2.jpg,threesixty_3.jpg"