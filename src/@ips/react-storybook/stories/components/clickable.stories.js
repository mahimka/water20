import React from 'react';

import { storiesOf } from '@storybook/react';
// import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';

import DText from '@ips/react/components/dtext';
import Slice from '@ips/react/components/slice';
import Clickable from '@ips/react/components/clickable';
import Readme from './clickable.md';

const addFn = 'addWithJSX'

storiesOf('Components/Clickable', module)
  .add('Doc', doc(Readme))
  
  [addFn]('(default)', () => (
    <div>
        <Clickable name="$0" once>This is a Clickable text. Click it!</Clickable>
        <DText text="expr (event('$0', 'click') && !(event('$1', 'click'))) ? 'clicked' : 'unclicked'; 'nothing'"/>
        <Clickable name="$1">Reset</Clickable>
    </div>
  ))

