import React from 'react';

import { storiesOf } from '@storybook/react';
// import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';

import Slice from '@ips/react/components/slice';
import Counter from '@ips/react/components/counter';
import Readme from './counter.md';

const addFn = 'addWithJSX'

storiesOf('Components/Counter', module)
  .add('Doc', doc(Readme))
  
  [addFn]('(default)', () => (
    <Counter name="$0" target={ 100 }/>
  ))

