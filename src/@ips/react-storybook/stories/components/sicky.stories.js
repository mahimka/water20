import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';


import Readme from './sticky.md';

import Slice from '@ips/react/components/slice';
import Sticky from '@ips/react/components/sticky';

// import './slice.styl'

import { lorem, lorem2 } from '../lorem.js'

const addFn = 'addWithJSX'


storiesOf('Components/Sticky', module)
  .add('Doc', doc(Readme))

storiesOf('Components/Sticky', module)
  [addFn]('(default)', () => (
    <div>
      <Slice height="50vh">{ lorem2 }</Slice>
      <Slice height="100vh"><Sticky>{ lorem }</Sticky></Slice>
      <Slice height="100vh">{ lorem2 }</Slice>
    </div>
  ))
