import React, { Component } from 'react'

import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withReadme, withDocs, doc } from 'storybook-readme'


import Readme from './slice.md'

import Text from '@ips/react/components/text'

import './slice.styl'

import { lorem2Rus } from '../lorem.js'

import { Slider, Checkbox } from 'element-react/next'
import 'element-theme-default/lib/checkbox.css'
import 'element-theme-default/lib/slider.css'
import '../ui.styl'

storiesOf('Components/Text', module)
  .add('Doc', doc(Readme))
  .addDecorator(withInfo)

storiesOf('Components/Text', module)
  .addWithJSX('(default)', () => (
    <Text text={ lorem2Rus }/>
  ))

storiesOf('Components/Text', module)
  	.addWithJSX('With options', ()=> {

        class TextOptions extends Component {

            state = { 
                carryUnions:true, 
                syllabify:true, 
                justify:true, 
                hyphenate:true, 
                hyphenLimit:1
            }

            onChangeCarryUnions = carryUnions =>{
                // trace('onChangeCarryUnions', carryUnions)
                this.setState({ carryUnions })
            }

            onChangeJustify = justify =>{
                // trace('onChangeJustify', justify)
                this.setState({ justify })
            }

            onChangeSyllabify = syllabify =>{
                // trace('onChangeSyllabify', syllabify)
                this.setState({ syllabify })
            }

            onChangeHyphenLimit = hyphenLimit =>{
                // trace('onChangeHyphenLimit', hyphenLimit)
                this.setState({ hyphenLimit })
            }

            render(){

                const { carryUnions, syllabify, justify, hyphenLimit } = this.state

                return <div>
                            <div className="options">
                                <div className="option">
                                    <span>Исправлять "висячие" предлоги</span>
                                    <Checkbox checked={ carryUnions } onChange={ this.onChangeCarryUnions }/>
                                </div>
                                <div className="option">
                                    <span>Выравнивать</span>
                                    <Checkbox checked={ justify } onChange={ this.onChangeJustify }/>
                                </div>
                                <div className="option">
                                    <span>Разбивать по слогам</span>
                                    <Checkbox checked={ syllabify } onChange={ this.onChangeSyllabify }/>
                                </div>                                
                                <div className="option">
                                    <span>Лимит переносов</span>
                                    <Slider value={ hyphenLimit } onChange={ this.onChangeHyphenLimit }/>
                                </div>
                                <div className="clear"/>
                            </div>

                            <Text 
                                text={ lorem2Rus } 
                                carryUnions={ carryUnions } 
                                syllabify={ syllabify } 
                                justify={ justify } 
                                hyphenLimit={ hyphenLimit }/>
                        </div>
            }
        }

        return <TextOptions/>
    })
