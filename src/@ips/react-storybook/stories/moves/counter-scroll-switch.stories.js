import React from 'react';

import { storiesOf } from '@storybook/react';
//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

import Way from   '@ips/react/components/way';
import Waypoint from   '@ips/react/components/waypoint';
import Counter from   '@ips/react/components/counter';

const addFn = 'addWithJSX'

storiesOf('Moves', module)
  [addFn]('Easing Counter by scroll', () => (
    <div style={{ height: '200vh' }}>
      <Way name="way"/>
      <Counter style={{ position: 'fixed' }} target="expr event('way', 'point', 'index')*100"/>
      <div style={{ height:"50vh" }}/>
      <Waypoint way="way"/>
      <div style={{ height:"30vh" }}/>
      <Waypoint way="way"/>
      <div style={{ height:"100vh" }}/>
    </div>
  ))

  [addFn]('Tweening Counter by scroll', () => (
    <div style={{ height: '200vh' }}>
      <Way name="way"/>
      <Counter style={{ position: 'fixed' }} current="expr tween(current, at([0, 100, 30, 60], event('way', 'point', 'index')+1), 1, 'Cubic.Out', [0])"/>
      <div style={{ height:"50vh" }}/>
      <Waypoint way="way"/>
      <div style={{ height:"30vh" }}/>
      <Waypoint way="way"/>
      <div style={{ height:"100vh" }}/>
      <Waypoint way="way"/>
      <div style={{ height:"100vh" }}/>
    </div>
  ))

