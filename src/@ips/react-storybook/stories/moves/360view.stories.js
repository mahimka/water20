import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';

import Slice from   '@ips/react/components/slice';


//import Readme from './image-sequence.md';

import ImageSequence from '@ips/react/components/image-sequence';
import DragTracker from  '@ips/react/components/drag-tracker';

const addFn = 'addWithJSX'

import threesixty from '../threesixty'


// storiesOf('Solutions', module)
//   .add('Info', doc(Readme))
//   //.add('Info', withReadme(Readme, ()=><div dangerouslySetInnerHTML={{ __html:info }}/>))
//   // .addDecorator(withInfo)


storiesOf('Moves', module)
  [addFn]('360view', () => (
  	<div>
		<Slice>
			<h1>Use a mouse to drag this</h1>
		</Slice>

	    <DragTracker name="dt1" integer range="0, 179" loop>
	        <ImageSequence current="expr event('dt1', 'drag', 'position')" images={ threesixty }/>
	    </DragTracker>
	</div>
  ), {
    // info:infoStyle,
  })
