import React from 'react'

import { storiesOf } from '@storybook/react'
//import { withReadme, withDocs, doc } from 'storybook-readme'
//import Readme from './image-sequence.md'

import ScrollTracker from   '@ips/react/components/scroll-tracker'
import DragTracker from   '@ips/react/components/drag-tracker'
import Video from   '@ips/react/components/video'
import Fixed from   '@ips/react/components/fixed'
import Slice from   '@ips/react/components/slice'

import './video-sequence.stories.styl'

const ffmpeg = 'Video file should be compressed with these options: ffmpeg -i in.mp4 -c:v libx264 -x264opts keyint=1 out.mp4'

storiesOf('Moves', module)
  .addWithJSX('VideoSequence + ScrollTracker', () => (
    <ScrollTracker name="vsst" throttle="0">
      <Slice>
        <div className="ffmpeg-instr">{ ffmpeg }</div>
        <Fixed cover>
          <Video srcId="1_15fps_keyint1" currentTime="expr (event('vsst', 'update', 'pos')||0)*15"/>
        </Fixed>
        
        <div style={{ height:'400vh' }}/>
      </Slice>
    </ScrollTracker>
  ))

storiesOf('Moves', module)
  .addWithJSX('VideoSequence + DragTracker', () => (
      <Slice>
        <div className="ffmpeg-instr">{ ffmpeg }</div>
        <Fixed cover>
          <Video srcId="castle_butte_with_silence_q12" currentTime="expr (event('vsdt', 'drag', 'position')||0)*0.01"/>
        </Fixed>
        <Fixed cover>
       
        <DragTracker mode="immediate" name="vsdt" range="0, 1000" speedMultiplier="1">
            THIS IS A CONTROLLER. DRAG OVER IT TO CONTROL THE VIDEO
        </DragTracker>
        </Fixed>

      </Slice>
  ))
