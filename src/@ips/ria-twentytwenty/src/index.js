// import 'polyfills'
import '@ips/app/trace'
import app from '@ips/app/app'
import appInject from '@ips/app/app-inject'
import * as analytics from './analytics'

import { render } from './ria-twentytwenty'

const prj = require('../project.json')
app.projectName = prj.name
app.title = prj.title
app.appMode = 'longread'

const inopts = {
    project: prj.name,
    scriptName: 'index.js',
    title: prj.title,
    description: prj.description,
    url: prj.url,
    YMID: prj.YMID,
}

// trace('render', ml)

const doInject = ()=>{
    appInject((container, opts)=>{
        app.publicPath = opts.publicPath
        opts.mode = 'inline-ria'

        analytics.connect(opts, { log:true })
        analytics.entrance()

        // if(opts.layoutArticle)
        //     opts.layoutArticle.style.paddingTop = '0px' // HARD HACK

        render(container, opts)
    }, inopts)
}

doInject()
