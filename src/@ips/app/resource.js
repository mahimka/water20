import { localizeUrl } from './utils'

let localize = true

export function selLocalize(is){
    localize = is
}

export function requestUrl(url, type, opts){
    // trace('requestUrl', url, type)

    // const a = [ ...opts ] // this works
    // const a = { ...opts } // this doesnt
    if(!localize)
        return url

    switch(type){
        case 'image':{
            return localizeUrl(url, Object.assign({}, { path: 'img', usePrefixDir:true }, opts))
        }
        case 'video':{
            return localizeUrl(url, Object.assign({}, { path: 'media', usePrefixDir:false }, opts))
        }
        case 'json':{
            return localizeUrl(url, Object.assign({}, { path: '', usePrefixDir:false }, opts))
        }
        case 'lottie':{
            return localizeUrl(url, Object.assign({}, { path: '', usePrefixDir:false }, opts))
        }
        default:
            return localizeUrl(url, Object.assign({}, { path: '', usePrefixDir:false }, opts))
    }
}
