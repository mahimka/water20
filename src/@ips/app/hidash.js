export const ud = o=>(typeof o === "undefined")
export const nop = ()=>{}
export const ident = v=>v
export const isString = o=>(typeof o === 'string' || o instanceof String)
export const isNumber = o=>(typeof o === 'number' || o instanceof Number)
export const isObject = o=>((typeof o === "object") && (o !== null))
export const isArray = o=>Array.isArray(o)
export const isFunction = o=>(typeof o === 'function' || o instanceof Function)
export const objEach = (o, f)=>{ Object.keys(o||[]).forEach((key)=>f(o[key], key)); return o }
export const objMap = (o, f)=>Object.keys(o||[]).map((key, i)=>f(o[key], key, i))
export const objTransform = (o, f)=>keyBy(Object.keys(o||[]).map( key =>[key, f(o[key], key)]), v=>v[0], v=>v[1])
// TODO export const objFilter = (o, f)=>
export const keyBy = (array, keyf = v=>v, valf=v=>v) => (array || []).reduce((acc, v, i)=>({ ...acc, [ keyf(v, i) ]:valf(v, i) }), {})
export const keyArr = (array, keyf = v=>v, valf=v=>v) => (array || []).reduce((acc, v, i)=>({ ...acc, [ v[0] ]:v[1] }), {})
export const times = (c, f)=>(Array.from({length: c}, (_,x) => f(x)))
export const each = (c, f)=>(c||[]).forEach(f)
export const map = (c=[], f)=>c.map(f)
export const filter = (c=[], f)=>c.filter(f)

/* native find for non-lame browsers, custom find for IE */
export const find = ud(document.documentMode) ? 
    (o, f)=>o.find(f) : 
    (o, f)=>{
        for(let i = 0; i < o.length; i++){
            const res = f(o[i], i)
            if(res)
                return o[i]
        }
    }
