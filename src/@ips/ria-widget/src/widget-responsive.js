import widgetInject from './widget-inject'
import css from './widget-responsive.styl'
import widgetStyleInject from './style-inject'
import * as Metrika from '@ips/app/metrika'

// import { findAncestor } from '@ips/app/app-inject-utils'

// let svgIcon = `<svg width="29" height="25" viewBox="0 0 29 25" fill="none" xmlns="http://www.w3.org/2000/svg">
//                 <path d="M27.369 10.2025L17.4727 0.875L15.5227 2.955L24.1027 11.08H0.458984V13.9237H24.1352L15.5227 22.0487L17.4727 24.1287L27.369 14.8175C27.731 14.5662 28.0268 14.2309 28.2311 13.8404C28.4353 13.4499 28.542 13.0157 28.542 12.575C28.542 12.1343 28.4353 11.7001 28.2311 11.3096C28.0268 10.9191 27.731 10.5838 27.369 10.3325V10.2025Z" fill="#fff"/>
//               </svg>`
//  <div class="ips-wi-cta-arrow">${svgIcon}</div>

import tpl from './widget-default-tpl'

function hasClass(element, className) {
    var regex = new RegExp('\\b' + className + '\\b');
    do {
      if (regex.exec(element.className)) { return true; }
      element = element.parentNode;
    } while (element);
    return false;
  }

widgetInject((container, opts)=>{
    console.log('widgetInject', opts)
    if(opts.img1x1) {
        if(hasClass(container, 'mod-shape-2x1')) { opts.img = opts.img1x1  }
        if(hasClass(container, 'mod-shape-2x2')) { opts.img = opts.img1x1  }
        if(hasClass(container, 'mod-shape-3x2')) { opts.img = opts.img1x1  }
        // if( findAncestor(container, $e => $e.classList.contains('article__infographics')) ) { opts.img = opts.img1x1  }
        if( hasClass(container, 'article__infographics')||hasClass(container, 'article__infographics_wtf') ) { opts.img = opts.img1x1  }
            
    }

    container.style.height = '100%'
    container.style.width = '100%'

    var cc = document.createElement('div')
    cc.style.height = '100%'
    cc.innerHTML = tpl(opts)

    var cca = document.createElement('div')
    cca.href = opts.url
    cca.style.height = '100%'
    cca.target= "_blank"
    cca.appendChild(cc)

    var w = cca
    container.appendChild(w)

    widgetStyleInject(css)

    // Yandex.Metrika    
    Metrika.init(opts)
    Metrika.event('widget_show')
    const link = cc.querySelector('.ips-wi-link')
    if(link)
        link.addEventListener('click', ()=> Metrika.event('widget_click'))    
})
