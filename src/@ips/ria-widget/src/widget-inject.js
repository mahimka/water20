import { xhrGetText } from '@ips/app/xhr'
import appInject from '@ips/app/app-inject-external'

const makeHTML = (s)=>{
    var el = document.createElement('div')
    el.innerHTML = s
    return el
}

const metanp = (p, d)=> {
    var el = (d||document).querySelector(`meta[name='${ p }']`)
    if(!el)
        el = (d||document).querySelector(`meta[property='${ p }']`)
    return el && el.getAttribute('content')
}

const defaultRender = (container, opts)=>{
    const a = document.createElement('a')
    a.href = opts.url
    a.target = '_blank'
    a.innerText = opts.title
}

export default render => 
    appInject((container, opts)=>{
        xhrGetText(
            [opts.publicPath, 'index.html'].join('/'),
            res => {

                render = render||defaultRender

                const index = makeHTML(res)

                const xopts = {
                    ...opts,
                    project: metanp('ria:project', index) || opts.project,
                    button: metanp('ria:widgetButton', index)||'Читать',
                    title: metanp('ria:title', index) || metanp('og:title', index) || (index.querySelector('title')||{}).innerText || opts.publicPath,
                    desc: metanp('ria:description', index) || metanp('og:description', index) || metanp('description', index) || '',
                    img: metanp('ria:image', index) || metanp('og:image', index) || metanp('twitter:image', index),
                    img1x1: metanp('ria:image1x1', index) || metanp('ria:image', index),
                    url: metanp('ria:url', index) || metanp('og:url', index) || metanp('twitter:url', index) || opts.publicPath,
                    YMID: metanp('metrika:id', index),
                }

                console.log('mounting widget', xopts, 'at', container)
                render(container, xopts)
            })
    })
