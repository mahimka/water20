import widgetInject from './widget-inject'
import css from './widget-responsive.styl'
import widgetStyleInject from './style-inject'

import * as Metrika from '@ips/app/metrika'

// let svgIcon = `<svg width="29" height="25" viewBox="0 0 29 25" fill="none" xmlns="http://www.w3.org/2000/svg">
//                 <path d="M27.369 10.2025L17.4727 0.875L15.5227 2.955L24.1027 11.08H0.458984V13.9237H24.1352L15.5227 22.0487L17.4727 24.1287L27.369 14.8175C27.731 14.5662 28.0268 14.2309 28.2311 13.8404C28.4353 13.4499 28.542 13.0157 28.542 12.575C28.542 12.1343 28.4353 11.7001 28.2311 11.3096C28.0268 10.9191 27.731 10.5838 27.369 10.3325V10.2025Z" fill="#fff"/>
//               </svg>`
//  <div class="ips-wi-cta-arrow">${svgIcon}</div>

import tpl from './widget-default-tpl'

widgetInject((container, opts)=>{

    if(opts.img1x1) {
        //if( hasClass(container, 'article__infographics')||hasClass(container, 'article__infographics_wtf') ) 
        if(opts.injectAsInfographics){
            opts.img = opts.img1x1
        }
    }

    var cc = document.createElement('div')
    cc.style.height = '100%'
    cc.innerHTML = tpl(opts, 'ips-wi-inject')

    container.style.height = '100%'
    container.appendChild(cc)

    widgetStyleInject(css)

    // google analytics
    /*
    (function() {
        var ifrm = document.createElement('iframe');
        ifrm.setAttribute('src', 'https://www.googletagmanager.com/ns.html?id=GTM-T2NZ6V');
        ifrm.setAttribute('height', '0');
        ifrm.setAttribute('width', '0');
        ifrm.style = 'display:none;visibility:hidden';
        container.appendChild(ifrm);
    })();
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
        'gtm.start': new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T2NZ6V');
    */

    // Yandex.Metrika
    Metrika.init(opts)
    Metrika.event('widget_show')
    const link = cc.querySelector('.ips-wi-link')
    if(link)
        link.addEventListener('click', ()=> Metrika.event('widget_click'))
})