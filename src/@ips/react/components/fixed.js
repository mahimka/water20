import React from 'react'
import Base from './base'
import './fixed.styl'

const ud = v => 'undefined' == typeof(v)

export default class Fixed extends Base{
    render(){
        const { style, children, cover, h100, w100 } = this.props

        const p = this.props
        const s = this.state

        // trace('fixed', this)

        return  <div className={ `fixed ${ !ud(s.className) ? s.className : (p.className||'') } ${ cover ? 'fixed_cover': '' } ${ w100 ? 'fixed_w100': '' } ${ h100 ? 'fixed_h100': '' } ` } style={ style }>
                    { children }
                </div>
    }
}
