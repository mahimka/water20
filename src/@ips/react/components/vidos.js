import React, { Component, useState, useContext, useEffect, useRef } from 'react'
import { useMemo, useCallback } from 'use-memo-one'

import { useExprContext } from '@ips/react/components/utils/react-expr'
import { usePreload } from "@ips/react/components/preload"
import useIntersectionObserver from "@ips/react/components/utils/use-intersection-observer"
import { ActivationContext } from "@ips/react/components/activation"
import __, { nop } from "@ips/app/hidash"

import { requestUrl } from '@ips/app/resource.js'

import './vidos.styl'

const srcUrl = v => v ? (v + '.mp4') : undefined
const posterUrl = v => v ? (v + '.jpg') : undefined
const posterFromSrc = v => v ? (v.split('.').slice(0, -1).join('.') + '.jpg') : undefined

const _setupVideoHandlers = (videoEl, opts = {})=>{
    // trace('_setupVideoHandlers', videoEl)
    if(!videoEl) return

    const { onCanplay, 
            onFinish, 
            onProgress, 
            playing,
            progressThrottle = .5 
        } = opts

    // trace('svh', playing)

    // const { onFinish = nop, onCanplay = nop, autoPlay } = props

    let canplayHandlers = []
    let endHandlers = []
    let progressHandlers = []

    // setTimeout(()=>{ 
    //     debugMsg('runnong ' + videoEl.readyState)
    //     trace('runnong', videoEl.readyState)
    // }, 0)
    // videoEl.addEventListener('readystatechange', ()=>{
    //     debugMsg('readystatechange ' + videoEl.readyState)
    //     trace('readystatechange', videoEl.readyState)
    //     if(videoEl.readyState >= 3)
    //         videoEl.play()
    // })

    if(onCanplay){
        canplayHandlers.push(onCanplay)
        videoEl.addEventListener('canplay', onCanplay)
    }

    if(onFinish){
        endHandlers.push(onFinish)
        videoEl.addEventListener('ended', onFinish)
    }

    if(onProgress && !videoEl.paused){
        progressHandlers.push(setInterval(()=>onProgress(videoEl.currentTime, videoEl.duration), progressThrottle*1000))
    }

    return ()=>{
        // cleanup listeners
        progressHandlers.forEach(h=>clearInterval(h))
        endHandlers.forEach(h=>videoEl.removeEventListener('ended', h))
        canplayHandlers.forEach(h=>videoEl.removeEventListener('canplay', h))
    }
}


const _setVideoPlay = (videoEl, play)=>{
    if(!videoEl) return

    if(play)
        videoEl.play()
    else
        videoEl.pause()
}


const _setVideoMuted = (videoEl, muted)=>{
    if(!videoEl) return
    videoEl.muted = muted
}

const _setVideoCurrentTime = (videoEl, t)=>{
    if(!videoEl) return
    videoEl.currentTime = t
}

const _rewindVideo = videoEl=>videoEl.currentTime = 0

export default p => {

    // const preloadCtx = useContext(PreloadContext)
    // const { preload, preloader } = preloadCtx
    // trace('preloadCtx', preloadCtx)

    const ref = useRef()

    const src = requestUrl(srcUrl(p.srcId), 'video')
    const poster = requestUrl(posterUrl(p.srcId), 'video')

    // Running preload
    const posterLoaded = usePreload(poster, 'image')
    const loaded = usePreload(src, 'video')

    const visible = useIntersectionObserver(ref.current)

    const [ playing, setPlaying ] = useState(false)
    const [ finished, setFinished ] = useState(false)

    const [ useExpr ] = useExprContext() // create a new Expr Context and get a custom useExpr hook from it
    const className = useExpr('className', p.className)

    // Setting up events
    const onFinish = useCallback(()=>{
        // trace('///////////////// onfinish //////////')
        p.onFinish()
        setFinished(true)
        // if(!p.loop)
        //     setPlaying(false)
    }, [
        p.onFinish, 
        p.loop
    ])

    const onCanplay = useMemo(()=>
        p.onCanplay?()=>p.onCanplay({
            ref:ref.current,
            setProgress: p=>{
                _setVideoCurrentTime(ref.current, p)
            },
            progress: ()=>ref.current?ref.current.currentTime:null,
            duration: ()=>ref.current?ref.current.duration:null,
        }):null
    ,[ p.onCanplay ])

    useEffect(()=>_setupVideoHandlers(ref.current, { ...p, onCanplay, onFinish }),[
        ref.current,
        playing,
        finished,
        onCanplay,
        onFinish,
        p.onProgress,
        p.progressThrottle,
    ])

    // Updating 'autoPlay' logic
    useEffect(()=>{
        // trace(`autoPlay logic loaded:${loaded} playing:${playing} autoPlay:${p.autoPlay} finished:${finished} visible:${visible}`)

        if(!loaded || !ref.current) return

        const letsplay = p.autoPlay && visible

        if(!playing && letsplay){
            if(p.rewind){
                _rewindVideo(ref.current)
                if(finished)
                    setFinished(false)
            }
        }

        if(!playing && !finished && letsplay){
            _setVideoPlay(ref.current, true)
            setPlaying(true)
        }

        if(playing && !letsplay){
            _setVideoPlay(ref.current, false)
            setPlaying(false)
        }

    },[
        ref.current,
        loaded,
        playing,
        visible,
        finished,
        p.autoPlay,
        p.rewind,
        p.loop,
    ])

    // trace('updating vid', p.muted)
    useEffect(()=>{
        if(!loaded || !ref.current) return
        // trace('updating muted', p.muted)
        _setVideoMuted(ref.current, p.muted)
    },[
        ref.current,
        loaded,
        p.muted,
        playing,
    ])



    // Rendering
    const renderVideo = useCallback(
        ()=><video 
                ref={ref} 
                className="video" 
                playsInline 
                //autoPlay={p.autoPlay}
                muted={p.muted} 
                loop={p.loop} 
                controls={p.controls}
                src={loaded?src:null} 
                poster={poster}/>
        ,[
            ref,
            loaded,
            // p.autoPlay,
            p.muted,
            p.loop,
            src,
            poster,
        ])

    // trace('susbasm', ref, loaded)
    if(!loaded && !posterLoaded) return null

    return (p.mode == 'background' ? 
                (<div className={`video-frame mode-background ${className}`}>
                    {renderVideo()}
                </div>) : 
                    renderVideo()
            )
}

