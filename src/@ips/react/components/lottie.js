import React, { useRef, useContext, useState, useMemo, useCallback, useEffect, useLayoutEffect } from 'react';
import Base from './base'
// import PropTypes from 'prop-types';
import { requestUrl } from '@ips/app/resource'
import { request, register } from '@ips/app/app-registry'
import { loadScript } from '@ips/app/utils'
import app from '@ips/app/app'
import * as __ from '@ips/app/hidash'
import { loadJson } from '@ips/app/load-media'
import { useActivation } from './activation'
import { PreloadContext } from './preload'



import './lottie.styl'
// <style lang="stylus">

// .lottie
//     position absolute
//     top 0px
//     width 100%
//     height 100%
//     pointer-events none

// </style>

// const purl = url => `${ app.publicPath ? (app.publicPath + '/') : '' }${ url }`

// trace('looking for lottie script', purl('lib/lottie.min.js'))
loadScript('https://dc.ria.ru/ips/lib/lottie558.min.js').then(()=>{
// loadScript(purl('lib/lottie.min.js')).then(()=>{
    // trace('bodymovin loaded')
    register('bodymovin', bodymovin)
})

const buildLottie = async($el, props, animationData)=>{
    const { anim, speed, autoPlay, loop, loopstart, loopend } = props


    // const state = current
    await request(['bodymovin'])
    // trace('got bodymovin', anim)

    if(!animationData){  // if it hasnt been preloaded - load anyways
        const animDataUrl = requestUrl(`${ anim }.json`, 'lottie', { path:'', usePrefixDir:false })
        // trace('loading json anim', animDataUrl)
        animationData = await loadJson(animDataUrl)
    }

    // trace('got lottie data for', anim)

    trace('running lottie', anim, speed, loop, autoPlay)

    var params = {
        container: $el,
        renderer: 'svg',
        loop: loop,
        autoplay: false,
        animationData
        // path: animDataUrl
    }

    const animInstance = bodymovin.loadAnimation(params)
    // trace('animInstance', animInstance)

    // if(!__.ud(fixStroke)){
    //     const $svg = animInstance.renderer.svgElement
    //     if($svg){
    //         const mazod = [...$svg.querySelectorAll('path,line,circle')]
    //         mazod.forEach($el=>$el.setAttribute('stroke-width', fixStroke))

    //     }
    // }

    if(!autoPlay)
        setTimeout(()=>animInstance.goToAndStop(0),0)

    if(!__.ud(speed))
        animInstance.setSpeed(+speed)

    // animInstance.addEventListener('DOMLoaded', ()=>{
    //     // trace(`lottie ${ anim } loaded: ${ this.anim.getDuration(true) }frames, ${ this.anim.getDuration() }s`)
    // })

    if(props.responsive){
        animInstance.renderer.svgElement.setAttribute('width',"100%")
        animInstance.renderer.svgElement.setAttribute('height',"100%")
    }

    if(loopstart){
        animInstance.addEventListener('DOMLoaded', ()=>{
            const loopStart = loopstart||0
            const loopEnd = loopend||animInstance.getDuration(true)

            const inSeg = ()=> animInstance.playSegments([0, loopStart], true)
            const loopSeg = ()=> animInstance.playSegments([loopStart, loopEnd], true)

            inSeg()
            animInstance.addEventListener('complete', loopSeg)
        })
    }

    return animInstance
}


const destroyLottie = (anim)=>{
    if(!anim) return
    anim.destroy()
}

const makeInfo = (l, name)=>({
    ctl:l,
    name:()=>name,
    length:()=>l.totalFrames,
    fps:()=>l.frameRate,
    current:()=>l.currentFrame,
    layers:()=>l.renderer.layers,
    $root:()=>l.renderer.svgElement,
    $layer:(nm)=>{
        const la = l.renderer.elements.find(la=>la.data.nm == nm)
        // trace('lookn 4', nm, la)
        if(!la) return
        return la.transformedElement//l.renderer.layerElement.children[la.ind-1]
    }
})

const Naked = React.forwardRef((p, outref)=>{
    const { $container } = p
    // if(!$container)
    //     return null

    // const ref = useRef()
    const [ loading, setLoading ] = useState(false)
    const [ anim, setAnim ] = useState(null) // i.e. !loading
    const [ info, setInfo ] = useState(null)
    const [ active, setActive ] = useState(false)
    const ctxActive = useActivation()
    const ctxPreload = useContext(PreloadContext)
    const [current, setCurrent] = useState(0)

    trace('incantameria', p.anim, $container, info)

    // trace('LottieF', p)
    useLayoutEffect(()=>{
        try{
            (async()=>{
                try{
                    if(anim || loading) return
                    if(!$container) return
                    if(!p.anim && !p.animData) return
                        
                    setLoading(true)
                    trace('loading lottie', p.anim, p.animData)


                    let animationData = null
                    if(p.animData){
                        animationData = p.animData
                    }else
                    if(p.anim && ctxPreload.preload && ctxPreload.preloader){
                        const animDataUrl = requestUrl(`${ p.anim }.json`, 'lottie', { path:'', usePrefixDir:false })
                        try{
                            animationData = await ctxPreload.preloader(animDataUrl, 'json', ctxPreload.priority)
                        }catch(err){
                            error('lottieerr', err)
                        }
                        // trace('preloaded lottie', animDataUrl, animationData)
                    }
                    setLoading(false)

                    // if(!animationData)
                    //     return

                    const a = await buildLottie($container, p, animationData)
                    a.goToAndStop(0)
                    trace('have lottie', a, p.anim, p.animData)
                    setAnim(a)
                    const ainfo = makeInfo(a, p.name||p.anim)
                    setInfo(ainfo)

                    if(outref)
                        outref.current = ainfo.$root()

                    if(p.onLoad){
                        p.onLoad()
                    }
                    if(p.onInfo){
                        p.onInfo(ainfo)
                    }
                } catch(err){
                    error(err)                
                }
            })()

            return ()=>{
                try{
                    trace('DESTR', anim)
                    if(!anim)
                        return
                    anim.destroy()
                    setAnim(null)
                } catch(err){
                    error(err)                
                }
            }
        } catch(err){
            error(err)                
        }
    },
    [
        $container,
        anim,
        p.anim,
        p.animData,
    ])

    useMemo(()=>{
        if(!anim) return

        // trace('rememing', p.anim, ctxActive, active)

        if(ctxActive != active){
            // trace('changing lottie', anim, anim.isPaused, p.anim, ctxActive)
            if(ctxActive)
                anim.play()
            else
                anim.stop()
            setActive(ctxActive)
        }
    },[
        anim,
        // p.autoPlay,
        active,
        ctxActive,
        loading
    ])

    useMemo(()=>{
        if(!anim) return

        // trace('chebooriendola', p.current, current)

        if(current != p.current){
            setCurrent(p.current)
            anim.goToAndStop(p.current, true)
        }
    },[p.current, current])

    // const renderCon = useCallback(
    //     ()=><div ref={ref} className={`lottie ${ p.className||'' }`} style={p.style}/>,[
    //         ref.current,
    //         p.className
    //     ])

    // return renderCon()

    return useMemo(()=>($container&&info) ? React.Children.map(p.children, c=>
                React.cloneElement(c, { $container:info.$root(), info })) : null
    , [p.children, info, $container])
    //<div ref={ref} className={`lottie ${ p.className||'' }`} style={p.style}/>
})

const MountPoint = p=>{
    trace('amamam', p)
    const { $container, info, children, node } = p

    return useMemo(()=>($container&&info) ? React.Children.map(children, c=>React.cloneElement(c, { $container: info.$layer(node), info })): null, [children, info, $container, node])
}


const Lottie = React.forwardRef((p, outref)=>{
    const ref = useRef()
    const [ container, setContainer ] = useState()

    useEffect(()=>{
        if(ref.current != container){
            setContainer(ref.current)
        }
    },[ref.current, container])

    return <div ref={ref} className={`lottie ${ p.className||'' }`} style={p.style}>
                <Naked ref={outref} $container={container} {...p}/>
            </div>
})

Lottie.displayName = 'Lottie'
Naked.displayName = 'Lottie.Naked'

//<Lottie anim="base">
//    <Lottie.Naked anim="char"/>
//    <Lottie.MountPoint node="char_mover">
//        <Lottie.Naked anim="char"/>
//    </Lottie.MountPoint>
//</Lottie>

export default Lottie
Lottie.LottieF = Lottie
Lottie.Naked = Naked
Lottie.MountPoint = MountPoint

        // props:['anim', 'speed', 'loop', 'loopstart', 'classname'],
            // lottieLib: (app.publicPath + '/lib/lottie.min.js'),
            // loop:true,
            // speed:1.,