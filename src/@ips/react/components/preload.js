import React, { useContext, useState, useEffect } from 'react'
import { useMemo } from 'use-memo-one'

export const PreloadContext = React.createContext({ 
    preload:false, 
    preloader:()=>{}, 
    isLoaded:()=>true 
})

export const preloadable = (S, ctx)=>(p=><PreloadContext.Provider value={ctx}><S {...p}/></PreloadContext.Provider>)

export const usePreload = (url, type, { skipErr=true, onLoad=()=>{} }={} )=>{
    const ctx = useContext(PreloadContext)

    const loadedBefore = useMemo(()=>ctx.isLoaded(url), [url, ctx])
    const [loaded, setLoaded] = useState(loadedBefore)

    // if the url has changed and it wasn't loaded before - reset loaded flag
    useEffect(()=>{
        if(!loadedBefore && loaded){
            setLoaded(false)
        }
    }, [url])

    useEffect(()=>{
        if(loaded)
            return

        if(!ctx.preload || !url){ // cant use preloader - just go on then
            setLoaded(true)
            onLoad(true)
            return
        }
        // trace('preloader', url);

        (async ()=>{ 
            try{
                const res = await ctx.preloader(url, type, ctx.priority)
                setLoaded(res||true) // default to true if nothing has returned
                onLoad(res||true)
            }catch(err){
                error(err)
                setLoaded(skipErr) // cant load - just go on then
            }
        })()

    }, [loaded, onLoad])

    return loaded
}

export const usePreloader= (Prelo, ...args)=>{
    return useMemo(()=>new Prelo(...args), [Prelo])
}
