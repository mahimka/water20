import React from 'react'
import Base from './base'
import { getFullOffsetTop, windowScrollY, parseOffset, windowSize } from '@ips/app/dom-utils'
import { request } from '@ips/app/app-registry'
//import { trackPoint } from './utils/vistracker'
import EE from '@ips/app/event-emitter'

import _ from 'lodash'

import { ActivationContext } from './activation'
// trace('ActivationContext', ActivationContext)

//trace('imported trackit', trackit)

// TODO: generate visboxes from one waypoint to another, covering the whole way
//
//


import './way.styl'

const ud = v => 'undefined' == typeof v

export default class Way extends Base{

    state = { points: [] }

    add(el, edge, top){
        // trace('added', el)

        let points = this.state.points||[]
        points.push({
            el,
            edge: ('undefined' == typeof edge) ? '0' : edge,
            top,
            // ofs: getFullOffsetTop(el),
        })

        // const ws = windowSize()

        // points = _.sortBy(_.map(points, p=>({ ofs: getFullOffsetTop(p.el) + parseOffset(p.edge), edge:p.edge, el:p.el })), 'ofs')
        // points = _.sortBy(_.map(points, p=>({ ...p, ofs: getFullOffsetTop(p.el) })), 'ofs')
        // this.setState({ points })
        this.rebuild(points)
    }

    remove(el){

    }

    recalc(props, state, force, prevContext){
        // trace('Way recalc', prevContext, this.context)
        let cprops = super.recalc(props, state, force)

        const { spread, spreadTrim, name } = props
        cprops.spreadPoints = _.times(spread, i =><div key={ i } className={`waypoint waypoint_over waypoint_${ name }-${ i+1 }`} style={{ top: `${ (i/(spread-(spreadTrim ? 1:0))*100)|0 }%` }}/>) 
        return cprops
    }

    render(){
        // trace('Way.render', this.activationContext)
        const { children, name, cover } = this.props
        const { spreadPoints } = this.cprops
        return (<div 
            ref={ ref=> this.$el = ref } 
            className={`way ${ cover ? 'way_cover':'' }`} 
            style={{ position:'relative', width:'0px' }} 
            data-name={ name }>
            { children }
            { spreadPoints }
        </div>)
    }

    async created(){
        super.created()

        const { target, spread, edge, name, continous, throttle } = this.props

        // trace('way', name, this)

        if(target){
            if(target[0] == '$') // it is a selector. otherwise - name (uid)
                this.targetEl = document.querySelector(target.substr(1))||window
            else{
                const r = await request([target])
                trace('way got the scroller', r)
                this.targetEl = r[target].$el||window
            }
        }else
            this.targetEl = window

        // _.times(spread, i =>{

        //     const sel = document.createElement('div')
        //     el.parentElement.appendChild(sel)
        //     points.push({
        //         sel,
        //         edge: ('undefined' == typeof edge) ? '0' : edge,
        //         top: i + '%',
        //         // ofs: getFullOffsetTop(el),
        //     })
        // })

        const points = _.map(this.$el.querySelectorAll('.waypoint'), (p, i)=>({
            el:p,
            edge:  ('undefined' == typeof edge) ? '0' : edge,
        }))
        if(points.length)
            this.rebuild(points)

        // this.targetEl = document.querySelector(target)||window
        const scr = this.targetEl != window ? ()=>this.targetEl.scrollTop : windowScrollY

        const scrolled = this.scrolledDiscrete//continous ? this.scrolledContinous : this.scrolledDiscrete
        scrolled(scr()) // set initial

        this._scrolled = _.throttle(()=>scrolled(scr()), ud(throttle) ? 200 : +throttle)
        this.targetEl.addEventListener('scroll', this._scrolled)
        window.addEventListener('resize', this.resize)

        // hardcore update sizes. TODO: rebuild this on MutationObserver or somn
        this.updateLoop = setInterval(()=>{
            const { points } = this.state
            this.rebuild(points)
        }, 2000)
    }

    // updated(prevProps, prevState, prevCtx){
    //     // trace('Way.updated', prevCtx, this.context)
    // }

    destroyed(){
        trace('Way.destroyed')
        super.destroyed()
        clearInterval(this.updateLoop)
        window.removeEventListener('resize', this.resize)
        this.targetEl.removeEventListener('scroll', this._scrolled)
    }

    scrolledDiscrete = s => {
        if(!this.context.active)
            return

        const { continous } = this.props
        const { points } = this.state

        // trace('scrolledDiscrete', this.props.name, this)
        for(var i = 0; i < points.length; i++){
            if(points[i].ofs > s)
                break;
        }

        i--

        const d = continous ? (()=>{
            if(i < 0 || i >= points.length - 1) return 0
            return (s - points[i].ofs)/(points[i+1].ofs - points[i].ofs)
        })() : 0

        const c = i + d

        if(this.current != c){
            this.current = c
            // if(this.state.current != i)
            //     this.setState({ current: i })

            trace(`way ${ this.props.name } updated ${ c }`)
            this.ee.fire('point', { index: i, continous: c })
        }
    }

    resize = _.throttle(()=>{
        // if(!this._isMounted)
        //     return

        const { points } = this.state
        this.rebuild(points)
    }, 500)

    rebuild(points){
        // if(!this._isMounted)
        //     return

        if(!this.context.active)
            return

        const { name, decorate } = this.props

        const wayEdge = parseOffset(this.props.edge)

        const riaOffset = Modernizr['topline-ria'] ? 40: (Modernizr['inline-ria'] ? 50 : 0)

        this.setState({ points:_.sortBy(_.map(points, p=>({ ...p, ofs: getFullOffsetTop(p.el) + wayEdge + parseOffset(p.edge) - riaOffset })), 'ofs') })

        if(decorate){
            // add decorations
            _.each(points, (p, i)=>{
                let deco = p.el.querySelector('.deco')
                if(!deco){
                    deco = document.createElement('div')
                    deco.className = 'deco'
                    p.el.appendChild(deco)
                }

                p.el.style.position = 'relative'

                // deco.style.top = parseOffset(p.edge) + 'px'
                const wpname = `${ name } - ${ i }`
                deco.innerText = wpname

                const ofs = parseOffset(p.edge) + wayEdge
                // deco.style.transform = `translateY(${ ofs })`
                deco.setAttribute('data-waypoint', wpname)

                deco.classList.toggle('neg', ofs > 0)


                deco.style.top = Math.min(ofs, 0) + 'px'
                deco.style.bottom = -Math.max(ofs, 0) + 'px'
                // deco.style.height = ofs
            })
        }
    }

}

Way.contextType = ActivationContext

