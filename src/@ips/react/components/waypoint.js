import React from 'react';
import Base from './base'
import { register, request } from '@ips/app/app-registry'
import { execute } from '@ips/app/actions'

export default class Waypoint extends Base{

    async created(){
        super.created()

        const { way, edge, top, spread } = this.props

        if(way){
            const res = await request([way]);
            //trace('waypoint got the way', res[way])
            this.way = res[way]
            res[way].add(this.$el, edge, top, spread)
        }
    }

    destroyed(){
        super.destroyed()
        
        if(this.way)
            this.way.remove(this.$el)
    }

    recalc(props, state){
        const { className } = props

        return {
            className: `waypoint ${ className||'' }`,
        }
    }

    render(){
        const { way } = this.props
        const { className } = this.cprops
        return  <div ref={ ref => this.$el = ref } data-way={ way||'default' } className={ className }/>
    }
}
