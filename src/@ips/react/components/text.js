import React, { Component, useState, useEffect } from 'react'
import { useMemo, useCallback } from 'use-memo-one'

import cx from '@ips/app/classnamex'
import carryUnions from '@ips/typo/carry-unions'
import { filterReadableEsc } from '@ips/typo/filter-readable'
// import shyify from '@ips/typo/shyify'
// import { createTextStyle, removeTextStyle } from '@ips/app/font-utils'

import { useTextStyle } from '@ips/react/components/utils/use-text-style'
import { useGridStyle, usePaddingStyle, layoutStyleCustom } from '@ips/react/components/layout'

import { createStyle, useStyle } from '@ips/react/components/utils/use-style'

const calcClassName = (p)=>`${p.classPrefx||''}text ${p.className||''} ${(p.mod||'').split(' ').map(s => `${p.classPrefx||''}text_${s}`).join(' ')}`

// const widthStyle = v=>(v&&!classyVal(v))?`width:${v};`:''
const widthStyle = v=>v?`width:${v};`:''
const noSelectStyle = v=>v?`user-select:none;`:''

const Text = p=>{

    const [textStyleClass] = useTextStyle(p.textStyle)

    const gridStyle = useGridStyle()
    const gutterClass = gridStyle.gutterClass()
    const [paddingClass] = usePaddingStyle(p.padding)

    const customStyle = useMemo(()=>(widthStyle(p.width)+noSelectStyle(p.noSelect)), Object.values(p))
    const [uClassName] = useStyle(layoutStyleCustom, customStyle)

    const text = useMemo(()=>carryUnions(filterReadableEsc(p.text||'')), [p.text])
    const className = useMemo(()=>cx(calcClassName(p), uClassName, textStyleClass, !(p.noGutter)&&gutterClass, paddingClass), [p.classPrefx, p.className, p.mod, textStyleClass, gutterClass, paddingClass])

    return <div className={className} style={p.style} onClick={p.onClick} dangerouslySetInnerHTML={{ __html: text }}/>
}
Text.displayName = 'Text'

export default Text
