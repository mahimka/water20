import React, { useRef, useLayoutEffect, forwardRef } from 'react'
import { useMemo } from 'use-memo-one'
import "./overlay.styl"

// import { register, unregister } from "@ips/app/app-registry"
import * as __ from '@ips/app/hidash'

import { requestUrl } from '@ips/app/resource'
import cx from '@ips/app/classnamex'
import { usePreload } from './preload'

import { useExprContext } from './utils/react-expr'
import { createStyle, useStyle } from '@ips/react/components/utils/use-style'

const overlayStyles = createStyle('ovl')

// export default class Overlay extends Component{

//     constructor(props){
//         super(props)
//         this.state = {}
//         this.cprops = this.recalc(props, this.state)
//     }

//     componentWillUpdate(props, state){
//         this.cprops = this.recalc(props, state)
//     }

//     componentDidUpdate(){
//     }

//     componentDidMount(){
//         if(this.props.name)
//             register(this.props.name, this)

//         const cs = getComputedStyle(this.$el.parentElement)
//         if(cs.position == 'static')
//             this.$el.parentElement.style.position = 'relative'

//         //trace('Overlay', this)
//     }

//     componentWillUnmount(){
//         if(this.props.name)
//             unregister(this.props.name, this)
//     }

//     recalc(props, state){
//         const { className, cover, sizeCover, vh100, w100, h100, tangible, img, mode, left, top, bottom, width, height, style, nolocalize } = props

//         const cprops = { style: style||{} }

//         cprops.className = `overlay ${ mode ? `overlay_${ mode }`:'' } ${ w100 ? `overlay_w100`:'' } ${ h100 ? `overlay_h100`:'' } ${ (cover||sizeCover) ? 'overlay_cover': '' } ${ vh100 ? 'overlay_vh100': '' } ${ tangible ? 'overlay_tangible': '' } ${ className||'' }`

//         if(img){
//             cprops.img = nolocalize ? img: requestUrl(img, 'image')
//             if(mode == 'background')
//                 cprops.style.backgroundImage = `url(${ cprops.img })`
//         }

//         cprops.style.left = left
//         cprops.style.top = top
//         cprops.style.width = width
//         cprops.style.height = height        
//         cprops.style.bottom = bottom

//         return cprops
//     }

//     render(){
//         const { mode, children } = this.props
//         const { className, style, img } = this.cprops

//         if(mode == 'img')
//             return <img ref={ ref=> this.$el = ref } className={ className } src={ img } style={ style }/>
//         else
//             return <div ref={ ref=> this.$el = ref } className={ className } style={ style }>{ children }</div>
//     }
// }

// const calcClassName = p=>`overlay ${ p.mode ? `overlay_${ p.mode }`:'' } ${ p.w100 ? `overlay_w100`:'' } ${ p.h100 ? `overlay_h100`:'' } ${ (p.cover||p.sizeCover) ? 'overlay_cover': '' } ${ p.vh100 ? 'overlay_vh100': '' } ${ p.tangible ? 'overlay_tangible': '' } ${ p.className||'' }`

// const calcStyle = p=>({
//     left: __.ud(p.right) ? p.left : 'auto',
//     right: p.right,
//     top: __.ud(p.bottom) ?  p.top : 'auto',
//     width: p.width,
//     height: p.height,
//     bottom: p.bottom,
//     backgroundColor:p.backgroundColor
// })

const posStyle = p=>`left:${__.ud(p.right) ? p.left : '0px'}; right: ${p.right}; top: ${__.ud(p.bottom) ?  p.top : '0px'}; ${(p.width||p.cover||p.w100)?`width: ${p.width||'100%'}`:''}; ${(p.height||p.cover||p.h100)?`height: ${p.height||'100%'}`:''}; bottom: ${p.bottom}`
const bkgSizeStyle = p=>p.mode=='background'?`background-size: ${__.ud(p.backgroundSize)?'cover':p.backgroundSize};`:''
const bkgPosStyle = p=>p.mode=='background'?`background-position: ${__.ud(p.backgroundPosition)?'center':p.backgroundPosition};`:''
const bkgRepeatStyle = p=>p.mode=='background'?`background-repeat: ${__.ud(p.backgroundRepeat)?'no-repeat':p.backgroundRepeat};`:''
const bkgMixStyle = p=>__.ud(p.mixBlendMode)?'':`mix-blend-mode: ${p.mixBlendMode};`
const bkgBlendStyle = p=>__.ud(p.backgroundBlendMode)?'':`background-blend-mode: ${p.backgroundBlendMode};`
const bkgImageStyle = (p, src, srcset, loaded)=>(p.mode=='background'&&src)?
        `background-image: url(${loaded?src:''}); background-image: -webkit-image-set(${loaded?srcset.map(s=>`url(${s[0]}) ${s[1]}`).join(' ,'):''});`
        :''
const bkgColorStyle = p=>__.ud(p.backgroundColor)?'':`background-color: ${p.backgroundColor};`

const overlayStyle = (p, src, srcset, loaded)=>`${bkgImageStyle(p, src, srcset, loaded)}${bkgColorStyle(p)}${bkgSizeStyle(p)}${bkgPosStyle(p)}${bkgRepeatStyle(p)}${bkgMixStyle(p)}${bkgBlendStyle(p)}${posStyle(p)}`

const OverlayF = forwardRef((p, ref)=>{

    const psrc = p.img||p.src
    const src = useMemo(()=>(psrc ? requestUrl(psrc, 'image') : null), [psrc])
    const srcset = useMemo(()=>{
        if(!src) return []
        const x = [[src, '1x']]
        if(p.res2x){
            const ss = src.split('.')
            const s0 = ss.slice(0,-1).join('.')
            x.push([s0+'@2x.'+ss[ss.length-1], '2x'])    
        }
        return x
    }, [src, p.res2x])


    const loaded = usePreload(src, 'image')
    // trace('OverlayF src', psrc, src, loaded)

    const [useExpr] = useExprContext()
    const eclassName = useExpr('className', p.className)
    // trace('eclassName', eclassName)

    // const style = useMemo(
    //     ()=>({
    //         ...calcStyle(p), 
    //         backgroundImage: (p.mode == 'background' ? `url(${ loaded?src:'' })`: null),
    //         backgroundSize: p.backgroundSize,
    //         backgroundPosition: p.backgroundPosition,
    //         mixBlendMode:p.mixBlendMode,
    //         backgroundBlendMode:p.backgroundBlendMode,
    //         backgroundRepeat: p.backgroundRepeat||'no-repeat',
    //         ...(p.style||{})
    //     }),
    //     [
    //         loaded,
    //         src,
    //         p.mode,
    //     ])

    const ostyle = useMemo(()=>overlayStyle(p, src, srcset, loaded), [
        loaded, 
        p.mode, 
        src, 
        srcset,
        p.backgroundColor,
    ])
    
    const [styleClass] = useStyle(overlayStyles, ostyle)
    

    const className = useMemo(
        ()=>cx(
            'overlay', 
            p.mode&&`overlay_${ p.mode }`,
            p.w100&&`overlay_w100`,
            p.h100&&`overlay_h100`,
            (p.cover||p.sizeCover)&&'overlay_cover',
            p.vh100&&'overlay_vh100',
            p.tangible&&'overlay_tangible',
            eclassName,
            styleClass
        ), 
        [
            p.mode,
            p.w100,
            p.h100,
            p.cover,
            p.sizeCover,
            p.vh100,
            p.tangible,
            eclassName,
            styleClass
        ])

    let myref = useRef()
    myref = ref||myref

    useLayoutEffect(()=>{
        if(!myref.current) 
            return
        const cs = getComputedStyle(myref.current.parentElement)
        if(cs.position == 'static')
            myref.current.parentElement.style.position = 'relative'
    })

    return useMemo(()=>((p.mode == 'img') ?
            <img ref={myref} className={ className } src={ loaded?src:null} style={ p.style } onClick={p.onClick}/>
        :
            <div ref={myref} className={ className } style={ p.style } onClick={p.onClick}>{ p.children }</div>), 
            [
                className,
                loaded,
                src,
                p.style,
                p.mode,
                p.children,
                p.onClick
            ])
})

export default OverlayF
OverlayF.OverlayF = OverlayF