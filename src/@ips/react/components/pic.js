import React, { Component, forwardRef } from 'react'
import { useMemo } from 'use-memo-one'

import { requestUrl } from '@ips/app/resource'
import { usePreload } from './preload'
import cx from '@ips/app/classnamex'

import { useGridStyle, usePaddingStyle, layoutStyleCustom } from '@ips/react/components/layout'
import { createStyle, useStyle } from '@ips/react/components/utils/use-style'

import './pic.styl'

import * as __ from '@ips/app/hidash'

// import Base from './base'
// export default class Pic extends Base{
//     render(){
//     const p = this.props
//         return <img className={ p.className || '' } style={ p.style } src={ requestUrl(p.src, 'image') }/>
//     }

// }

// const bkgStyle = p=>

const parseSize = v=>v == 'screen' ? '100vh' : v
const maxWidthStyle = v=>(v)?`max-width:${parseSize(v)};`:''
const widthStyle = v=>(v)?`width:${parseSize(v)};`:''
const heightStyle = v=>(v)?`height:${parseSize(v)};`:''
const maxHeightStyle = v=>(v)?`max-height:${parseSize(v)};`:''
const bkgImgStyle = (src, srcset, loaded)=>src?`background-image:url(${loaded?src:''});background-image: image-set(${loaded?srcset.map((s, i)=>`url(${s}) ${i+1}x`).join(' ,'):''});`:''
const aspectStyle = aspect=>aspect?`padding-bottom: ${aspect*100}%;`:''

const inStyle = (p, src, srcset, loaded)=>`${p.mode == 'background'&&src ? bkgImgStyle(src, srcset, loaded): ''}${!__.ud(p.height)?(heightStyle(p.iHeight||'100%')):widthStyle(p.iWidth||'100%')}`
const outStyle = (p, loaded)=>`${maxHeightStyle(p.maxHeight)}${maxWidthStyle(p.maxWidth)}${widthStyle(p.width)}${heightStyle(p.height)}${(p.mode=='background'|| !loaded) ? aspectStyle(p.aspect):''}`

const Pic = forwardRef((p, ref)=>{
    const src = useMemo(()=>requestUrl(p.src, 'image'), [p.src])
    const srcset = useMemo(()=>{
        if(!src) return []
        const x = [[src, '1x']]
        if(p.res2x){
            const ss = src.split('.')
            if(ss[ss.length-1] != 'svg'){ // skip res2x for svgs
                const s0 = ss.slice(0,-1).join('.')
                x.push([s0+'_2x.'+ss[ss.length-1], '2x'])    
            }
        }
        return x
    }, [src, p.res2x])


    const loaded = usePreload(src, 'image')
    // trace(`pic src ${src} loaded ${loaded}`, srcset)

    // const sizeClass = __.ud(p.height)?'hsize':'vsize'

    // const instyle = useMemo(()=>({
    //     backgroundImage: p.mode == 'background' ? `url(${ loaded?src:'' })`: undefined,
    // }), [loaded,src,p.mode])

    // const outstyle = useMemo(()=>({
    //     paddingBottom: p.aspect?`${p.aspect*100}%`:undefined
    // }), [p.aspect])

    const instyle = useMemo(()=>inStyle(p, src, srcset, loaded), [loaded, p.mode, src, srcset])
    const outstyle = useMemo(()=>outStyle(p, loaded), [p.maxWidth, p.maxHeight, p.aspect, p.mode, loaded])
    const [instyleClass] = useStyle(layoutStyleCustom, instyle)
    const [outstyleClass] = useStyle(layoutStyleCustom, outstyle)

    // trace('outstyle', outstyle)

    const gridStyle = useGridStyle()
    const gutterClass = gridStyle.gutterClass()
    const [paddingClass] = usePaddingStyle(p.padding)

    const className = useMemo(()=>cx('pic', outstyleClass, p.className, !(p.noGutter)&&gutterClass, paddingClass), [p.className, gutterClass, paddingClass])

    return useMemo(()=>((p.mode == 'background') ?
            <div ref={ref} className={className}>
                <div className={cx(instyleClass)}>
                </div>
            </div>
        :
            <div ref={ref} className={className} >
                <img className={cx(instyleClass)} src={ loaded?src:null } srcSet={srcset.map(s=>s.join(' ')).join(',')} draggable="false" onClick={p.onClick}/>
            </div>), 
            [
                className,
                loaded,
                src,
                instyleClass,
                outstyleClass,
                p.mode,
                p.children
            ])
})
Pic.displayName='Pic'

export default Pic