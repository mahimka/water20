import React from 'react'
import Base from './base'

import './parallax.styl'

export default class Parallax extends Base{
    render(){
        const { className,  children } = this.props
        return <div ref={ ref=> this.$el = ref } className={`parallax ${ className||'' }`}> 
                    { children } 
                </div>
    }
}
